# CongelatHeure

This project have for purpose to inventory your freezer.
It permits to know what you have in your freezer and if a food is going to expire.

To start to use it you need first add your foods.

<img src="/pictures/Screenshot_20220226-185725.jpg" alt="drawing" width="200"/>

You can specify the name, the limit date, the category of your food, the location of your food, the quantity and you can also add a photo.

<img src="/pictures/Screenshot_20220226-185738.jpg" alt="drawing" width="200" />

If there is not enough categories or locations you can add an another.

<img src="/pictures/Screenshot_20220226-185759.jpg" alt="drawing" width="200" />

There is an history which details all your actions you made.

<img src="/pictures/Screenshot_20220226-185840.jpg" alt="drawing" width="200" />


What used in main branch :
- Rxjava
- Dagger
- Room
- Material design
- Basic architecture

What used in others branches :
- Coroutines
- Hilt
- Clean architecture
