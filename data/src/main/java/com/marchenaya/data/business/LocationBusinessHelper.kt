package com.marchenaya.data.business

import com.google.gson.Gson
import com.marchenaya.data.entity.db.HistoryDBEntity
import com.marchenaya.data.entity.local.LocationEntity
import com.marchenaya.data.manager.db.DBManager
import com.marchenaya.data.mapper.db.LocationDBEntityDataMapper
import com.marchenaya.data.model.Type
import dagger.Reusable
import java.util.Calendar
import javax.inject.Inject

@Reusable
class LocationBusinessHelper @Inject constructor(
    private val dbManager: DBManager,
    private val locationDBEntityDataMapper: LocationDBEntityDataMapper
) {

    fun getLocationList(): List<LocationEntity> =
        locationDBEntityDataMapper.transformDBEntityList(dbManager.getLocationList())
            .sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.name })

    private fun getLocationListById(ids: List<Int>): List<LocationEntity> =
        locationDBEntityDataMapper.transformDBEntityList(dbManager.getLocationListById(ids))

    fun saveLocation(locationEntity: LocationEntity) {
        val locationId =
            dbManager.saveLocation(locationDBEntityDataMapper.transformEntityToDB(locationEntity))
        dbManager.saveHistory(
            HistoryDBEntity(
                type = Type.LOCATION,
                newObject = Gson().toJson(locationEntity.copy(id = locationId.toInt())),
                date = Calendar.getInstance().time
            )
        )
    }

    fun updateLocation(id: Int, name: String) {
        val oldLocationEntity = getLocation(id)
        val newLocationEntity = LocationEntity(id, name)
        dbManager.updateLocation(locationDBEntityDataMapper.transformEntityToDB(newLocationEntity))
        dbManager.saveHistory(
            HistoryDBEntity(
                type = Type.LOCATION,
                oldObject = Gson().toJson(oldLocationEntity),
                newObject = Gson().toJson(newLocationEntity),
                date = Calendar.getInstance().time
            )
        )
    }

    private fun removeLocations(ids: List<Int>) {
        val locations = getLocationListById(ids)
        dbManager.removeLocationsByIds(ids)
        locations.forEach {
            dbManager.saveHistory(
                HistoryDBEntity(
                    type = Type.LOCATION,
                    oldObject = Gson().toJson(it),
                    date = Calendar.getInstance().time
                )
            )
        }
    }

    private fun getLocation(id: Int): LocationEntity =
        locationDBEntityDataMapper.transformDBToEntity(dbManager.getLocationById(id))

    fun updateFoodsAndRemoveLocations(ids: List<Int>) {
        dbManager.updateFoodsToUndefinedLocation(ids)
        removeLocations(ids)
    }
}
