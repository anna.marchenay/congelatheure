package com.marchenaya.data.business

import com.marchenaya.data.entity.local.HistoryEntity
import com.marchenaya.data.extensions.convertToString
import com.marchenaya.data.manager.db.DBManager
import com.marchenaya.data.mapper.db.HistoryDBEntityDataMapper
import com.marchenaya.data.model.HistoryDescription
import com.marchenaya.data.model.Type
import dagger.Reusable
import javax.inject.Inject

@Reusable
class HistoryBusinessHelper @Inject constructor(
    private val dbManager: DBManager,
    private val historyDBEntityDataMapper: HistoryDBEntityDataMapper
) {

    fun getHistoryList(): List<HistoryEntity> =
        historyDBEntityDataMapper.transformDBEntityList(dbManager.getHistoryList())

    fun searchHistory(
        text: String,
        dayFormat: String,
        hourFormat: String,
        description: (historyDescription: HistoryDescription, type: Type, oldObject: String?, newObject: String?) -> String,
        historyDescription: HistoryDescription
    ): List<HistoryEntity> =
        getHistoryList().filter {
            (
                    Regex(text).containsMatchIn(it.date.convertToString(dayFormat)) ||
                            Regex(text).containsMatchIn(it.date.convertToString(hourFormat)) ||
                            Regex(text).containsMatchIn(
                                description(
                                    historyDescription,
                                    it.type,
                                    it.oldObject,
                                    it.newObject
                                )
                            )
                    )
        }

    fun saveHistory(historyEntity: HistoryEntity) =
        dbManager.saveHistory(historyDBEntityDataMapper.transformEntityToDB(historyEntity))
}
