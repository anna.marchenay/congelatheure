package com.marchenaya.data.business

import com.google.gson.Gson
import com.marchenaya.data.entity.db.HistoryDBEntity
import com.marchenaya.data.entity.local.CategoryEntity
import com.marchenaya.data.manager.db.DBManager
import com.marchenaya.data.mapper.db.CategoryDBEntityDataMapper
import com.marchenaya.data.model.Type
import java.util.Calendar
import javax.inject.Inject

class CategoryBusinessHelper @Inject constructor(
    private val dbManager: DBManager,
    private val categoryDBEntityDataMapper: CategoryDBEntityDataMapper
) {

    private fun getCategory(id: Int): CategoryEntity =
        categoryDBEntityDataMapper.transformDBToEntity(dbManager.getCategoryById(id))

    fun getCategoryList(): List<CategoryEntity> =
        categoryDBEntityDataMapper.transformDBEntityList(dbManager.getCategoryList())
            .sortedWith(compareBy(java.lang.String.CASE_INSENSITIVE_ORDER) { it.name })

    private fun getCategoryListById(ids: List<Int>): List<CategoryEntity> =
        categoryDBEntityDataMapper.transformDBEntityList(dbManager.getCategoryListById(ids))

    fun saveCategory(categoryEntity: CategoryEntity) {
        val categoryId =
            dbManager.saveCategory(categoryDBEntityDataMapper.transformEntityToDB(categoryEntity))
        dbManager.saveHistory(
            HistoryDBEntity(
                type = Type.CATEGORY,
                newObject = Gson().toJson(categoryEntity.copy(id = categoryId.toInt())),
                date = Calendar.getInstance().time
            )
        )
    }

    fun updateCategory(id: Int, name: String) {
        val oldCategoryEntity = getCategory(id)
        val newCategoryEntity = CategoryEntity(id, name)
        dbManager.updateCategory(categoryDBEntityDataMapper.transformEntityToDB(newCategoryEntity))
        dbManager.saveHistory(
            HistoryDBEntity(
                type = Type.CATEGORY,
                oldObject = Gson().toJson(oldCategoryEntity),
                newObject = Gson().toJson(newCategoryEntity),
                date = Calendar.getInstance().time
            )
        )
    }

    private fun removeCategories(ids: List<Int>) {
        val categories = getCategoryListById(ids)
        dbManager.removeCategoriesByIds(ids)
        categories.forEach {
            dbManager.saveHistory(
                HistoryDBEntity(
                    type = Type.CATEGORY,
                    oldObject = Gson().toJson(it),
                    date = Calendar.getInstance().time
                )
            )
        }
    }

    fun updateFoodsAndRemoveCategory(ids: List<Int>) {
        dbManager.updateFoodsToOtherCategory(ids)
        removeCategories(ids)
    }
}
