package com.marchenaya.data.business

import com.google.gson.Gson
import com.marchenaya.data.entity.db.HistoryDBEntity
import com.marchenaya.data.entity.local.FoodEntity
import com.marchenaya.data.manager.db.DBManager
import com.marchenaya.data.mapper.db.CategoryDBEntityDataMapper
import com.marchenaya.data.mapper.db.FoodDBEntityDataMapper
import com.marchenaya.data.mapper.db.LocationDBEntityDataMapper
import com.marchenaya.data.model.FoodSorter
import com.marchenaya.data.model.Type
import dagger.Reusable
import java.util.Calendar
import javax.inject.Inject

@Reusable
class FoodBusinessHelper @Inject constructor(
    private val dbManager: DBManager,
    private val foodDBEntityDataMapper: FoodDBEntityDataMapper,
    private val locationDBEntityDataMapper: LocationDBEntityDataMapper,
    private val categoryDBEntityDataMapper: CategoryDBEntityDataMapper
) {

    fun getFoodList(foodSorter: FoodSorter): List<FoodEntity> {

        val foodEntityList = mutableListOf<FoodEntity>()
        foodDBEntityDataMapper.transformDBEntityList(dbManager.getFoodList())
            .forEach { foodEntity ->
                val locationDBEntity = dbManager.getLocationById(foodEntity.locationEntity.id)
                val categoryDBEntity = dbManager.getCategoryById(foodEntity.categoryEntity.id)
                foodEntityList.add(
                    foodEntity.copy(
                        locationEntity = locationDBEntityDataMapper.transformDBToEntity(
                            locationDBEntity
                        ),
                        categoryEntity = categoryDBEntityDataMapper.transformDBToEntity(
                            categoryDBEntity
                        )
                    )
                )
            }
        return sortBy(foodSorter, foodEntityList)
    }

    fun getFood(id: Int): FoodEntity {
        val foodEntity = foodDBEntityDataMapper.transformDBToEntity(dbManager.getFood(id))
        val locationDBEntity = dbManager.getLocationById(foodEntity.locationEntity.id)
        val categoryDBEntity = dbManager.getCategoryById(foodEntity.categoryEntity.id)
        return foodEntity.copy(
            locationEntity = locationDBEntityDataMapper.transformDBToEntity(
                locationDBEntity
            ),
            categoryEntity = categoryDBEntityDataMapper.transformDBToEntity(categoryDBEntity)
        )
    }

    private fun getFoodListById(ids: List<Int>): List<FoodEntity> =
        foodDBEntityDataMapper.transformDBEntityList(dbManager.getFoodListById(ids))

    fun saveFood(foodEntity: FoodEntity) {
        val foodId = dbManager.saveFood(foodDBEntityDataMapper.transformEntityToDB(foodEntity))
        dbManager.saveHistory(
            HistoryDBEntity(
                type = Type.FOOD,
                newObject = replaceEntityToModelJson(Gson().toJson(foodEntity.copy(id = foodId.toInt()))),
                date = Calendar.getInstance().time
            )
        )
    }

    fun updateFood(foodEntity: FoodEntity) {
        val oldFoodEntity = getFood(foodEntity.id)
        dbManager.updateFood(foodDBEntityDataMapper.transformEntityToDB(foodEntity))
        dbManager.saveHistory(
            HistoryDBEntity(
                type = Type.FOOD,
                oldObject = replaceEntityToModelJson(Gson().toJson(oldFoodEntity)),
                newObject = replaceEntityToModelJson(Gson().toJson(foodEntity)),
                date = Calendar.getInstance().time
            )
        )
    }

    fun removeFoods(ids: List<Int>) {
        val foods = getFoodListById(ids)
        dbManager.removeFoodsByIds(ids)
        foods.forEach {
            dbManager.saveHistory(
                HistoryDBEntity(
                    type = Type.FOOD,
                    oldObject = replaceEntityToModelJson(Gson().toJson(it)),
                    date = Calendar.getInstance().time
                )
            )
        }
    }

    fun getFoodListWithCriteria(
        foodSorter: FoodSorter,
        filteredCategoryIdList: List<Int>,
        search: String
    ): List<FoodEntity> {
        var foodEntityList = getFoodList(foodSorter)
        if (filteredCategoryIdList.isNotEmpty() && search.isNotEmpty()) {
            foodEntityList =
                (foodEntityList.filter { filteredCategoryIdList.contains(it.categoryEntity.id) && it.name == search })
        } else if (filteredCategoryIdList.isNotEmpty()) {
            foodEntityList =
                (foodEntityList.filter { filteredCategoryIdList.contains(it.categoryEntity.id) })
        } else if (search.isNotEmpty()) {
            foodEntityList = (foodEntityList.filter { it.name == search })
        }
        return sortBy(foodSorter, foodEntityList)
    }

    private fun sortBy(foodSorter: FoodSorter, foodEntityList: List<FoodEntity>): List<FoodEntity> {
        return when (foodSorter) {
            FoodSorter.NAME_ASC -> {
                foodEntityList.sortedWith(
                    compareBy(
                        java.lang.String.CASE_INSENSITIVE_ORDER
                    ) { it.name }
                )
            }
            FoodSorter.NAME_DESC -> {
                foodEntityList.sortedWith(
                    compareByDescending(
                        java.lang.String.CASE_INSENSITIVE_ORDER
                    ) { it.name }
                )
            }
            FoodSorter.LIMIT_DATE_ASC -> {
                foodEntityList.sortedWith(compareBy(nullsLast()) { it.limitDate })
            }
            FoodSorter.LIMIT_DATE_DESC -> {
                foodEntityList.sortedWith(compareByDescending(nullsLast()) { it.limitDate })
            }
            FoodSorter.ADD_DATE_ASC -> {
                foodEntityList.sortedBy { it.addDate }
            }
            FoodSorter.ADD_DATE_DESC -> {
                foodEntityList.sortedByDescending { it.addDate }
            }
        }
    }

    private fun replaceEntityToModelJson(json: String): String {
        return json.replace("categoryEntity", "category")
            .replace("locationEntity", "location")
    }
}
