package com.marchenaya.data.repository

import com.marchenaya.data.business.LocationBusinessHelper
import com.marchenaya.data.mapper.local.LocationEntityDataMapper
import com.marchenaya.data.model.Location
import dagger.Reusable
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

@Reusable
class LocationRepository @Inject constructor(
    private val locationBusinessHelper: LocationBusinessHelper,
    private val locationEntityDataMapper: LocationEntityDataMapper
) {

    fun getLocationList(): Single<List<Location>> = Single.defer {
        Single.just(
            locationEntityDataMapper.transformEntityList(locationBusinessHelper.getLocationList())
                .sortedWith(
                    compareBy(
                        java.lang.String.CASE_INSENSITIVE_ORDER
                    ) { it.name }
                )
        )
    }

    fun saveLocation(name: String): Completable = Completable.defer {
        locationBusinessHelper.saveLocation(
            locationEntityDataMapper.transformModelToEntity(Location(name = name))
        )
        Completable.complete()
    }

    fun updateLocation(id: Int, name: String): Completable = Completable.defer {
        locationBusinessHelper.updateLocation(id, name)
        Completable.complete()
    }

    fun updateFoodsAndRemoveLocations(ids: List<Int>): Completable = Completable.defer {
        locationBusinessHelper.updateFoodsAndRemoveLocations(ids)
        Completable.complete()
    }
}
