package com.marchenaya.data.repository

import com.marchenaya.data.business.HistoryBusinessHelper
import com.marchenaya.data.mapper.local.HistoryEntityDataMapper
import com.marchenaya.data.model.History
import com.marchenaya.data.model.HistoryDescription
import com.marchenaya.data.model.Type
import dagger.Reusable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

@Reusable
class HistoryRepository @Inject constructor(
    private val historyBusinessHelper: HistoryBusinessHelper,
    private val historyEntityDataMapper: HistoryEntityDataMapper
) {

    fun getHistoryList(): Single<List<History>> = Single.defer {
        Single.just(
            historyEntityDataMapper.transformEntityList(historyBusinessHelper.getHistoryList())
        )
    }

    fun searchHistory(
        text: String,
        dayFormat: String,
        hourFormat: String,
        description: (historyDescription: HistoryDescription, type: Type, oldObject: String?, newObject: String?) -> String,
        historyDescription: HistoryDescription
    ): Single<List<History>> = Single.defer {
        Single.just(
            historyEntityDataMapper.transformEntityList(
                historyBusinessHelper.searchHistory(
                    text,
                    dayFormat,
                    hourFormat,
                    description,
                    historyDescription
                )
            )
        )
    }

}
