package com.marchenaya.data.repository

import com.marchenaya.data.business.FoodBusinessHelper
import com.marchenaya.data.mapper.local.FoodEntityDataMapper
import com.marchenaya.data.model.Food
import com.marchenaya.data.model.FoodSorter
import dagger.Reusable
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

@Reusable
class FoodRepository @Inject constructor(
    private val foodBusinessHelper: FoodBusinessHelper,
    private val foodEntityDataMapper: FoodEntityDataMapper
) {

    fun getFoodList(sort: FoodSorter = FoodSorter.NAME_ASC): Single<List<Food>> = Single.defer {
        Single.just(
            foodEntityDataMapper.transformEntityList(foodBusinessHelper.getFoodList(sort))
        )
    }

    fun getFood(id: Int): Single<Food> = Single.defer {
        Single.just(
            foodEntityDataMapper.transformEntityToModel(foodBusinessHelper.getFood(id))
        )
    }

    fun saveFood(food: Food): Completable = Completable.defer {
        foodBusinessHelper.saveFood(foodEntityDataMapper.transformModelToEntity(food))
        Completable.complete()
    }

    fun updateFood(food: Food): Completable = Completable.defer {
        foodBusinessHelper.updateFood(foodEntityDataMapper.transformModelToEntity(food))
        Completable.complete()
    }

    fun removeFoods(ids: List<Int>): Completable = Completable.defer {
        foodBusinessHelper.removeFoods(ids)
        Completable.complete()
    }

    fun getFoodListWithCriteria(
        sort: FoodSorter,
        filteredCategoryIdList: List<Int>,
        search: String
    ): Single<List<Food>> = Single.defer {
        Single.just(
            foodEntityDataMapper.transformEntityList(
                foodBusinessHelper.getFoodListWithCriteria(
                    sort,
                    filteredCategoryIdList,
                    search
                )
            )
        )
    }
}
