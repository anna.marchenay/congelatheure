package com.marchenaya.data.repository

import com.marchenaya.data.business.CategoryBusinessHelper
import com.marchenaya.data.mapper.local.CategoryEntityDataMapper
import com.marchenaya.data.model.Category
import dagger.Reusable
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

@Reusable
class CategoryRepository @Inject constructor(
    private val categoryBusinessHelper: CategoryBusinessHelper,
    private val categoryEntityDataMapper: CategoryEntityDataMapper
) {

    fun getCategoryList(): Single<List<Category>> = Single.defer {
        Single.just(
            categoryEntityDataMapper.transformEntityList(categoryBusinessHelper.getCategoryList())
        )
    }

    fun saveCategory(name: String): Completable = Completable.defer {
        categoryBusinessHelper.saveCategory(
            categoryEntityDataMapper.transformModelToEntity(
                Category(
                    name = name
                )
            )
        )
        Completable.complete()
    }

    fun updateCategory(id: Int, name: String): Completable =
        Completable.defer {
            categoryBusinessHelper.updateCategory(id, name)
            Completable.complete()
        }

    fun updateFoodsAndRemoveCategories(ids: List<Int>): Completable = Completable.defer {
        categoryBusinessHelper.updateFoodsAndRemoveCategory(ids)
        Completable.complete()
    }
}
