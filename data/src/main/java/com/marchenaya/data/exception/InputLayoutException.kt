package com.marchenaya.data.exception

class InputLayoutException(message: String) : RuntimeException(message)
