package com.marchenaya.data.model

import java.util.Date

data class Food(
    val id: Int = 0,
    val name: String,
    val limitDate: Date?,
    val addDate: Date,
    val modifiedDate: Date,
    val imagePath: String,
    val quantity: Int,
    val location: Location,
    val category: Category
)
