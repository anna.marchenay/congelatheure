package com.marchenaya.data.model

import java.util.Date

data class History(
    val id: Int = 0,
    val type: Type,
    val oldObject: String? = null,
    val newObject: String? = null,
    val date: Date
)
