package com.marchenaya.data.model

enum class Type {
    FOOD,
    LOCATION,
    CATEGORY
}
