package com.marchenaya.data.model

class Category(
    val id: Int = 0,
    val name: String
)
