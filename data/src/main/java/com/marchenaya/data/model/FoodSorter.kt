package com.marchenaya.data.model

enum class FoodSorter {
    NAME_ASC,
    NAME_DESC,
    LIMIT_DATE_ASC,
    LIMIT_DATE_DESC,
    ADD_DATE_ASC,
    ADD_DATE_DESC
}
