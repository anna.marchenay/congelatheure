package com.marchenaya.data.model

data class HistoryDescription(
    val historyCategoryModified: String,
    val historyCategoryRemoved: String,
    val historyCategoryAdded: String,
    val historyLocationModified: String,
    val historyLocationRemoved: String,
    val historyLocationAdded: String,
    val historyFoodModified: String,
    val historyFoodRemoved: String,
    val historyFoodAdded: String,
    val historyFoodModifiedName: String,
    val historyFoodModifiedLimitDate: String,
    val historyFoodModifiedNullLimitDate: String,
    val historyFoodModifiedImage: String,
    val historyFoodModifiedQuantity: String,
    val historyFoodModifiedCategory: String,
    val historyFoodModifiedLocation: String,
    val limitDateFormat: String,
)
