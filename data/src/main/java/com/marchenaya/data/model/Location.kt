package com.marchenaya.data.model

data class Location(
    val id: Int = 0,
    val name: String
)
