package com.marchenaya.data.di.module

import com.marchenaya.data.manager.db.DBManager
import com.marchenaya.data.manager.db.DBManagerImpl
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
object ManagerModule {

    @Provides
    @Reusable
    fun dbManager(dbManagerImpl: DBManagerImpl): DBManager = dbManagerImpl
}
