package com.marchenaya.data.di

import android.content.Context
import com.marchenaya.data.di.module.DataModule
import com.marchenaya.data.di.module.ManagerModule
import com.marchenaya.data.repository.CategoryRepository
import com.marchenaya.data.repository.FoodRepository
import com.marchenaya.data.repository.HistoryRepository
import com.marchenaya.data.repository.LocationRepository
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DataModule::class, ManagerModule::class])
interface DataComponent {

    fun locationRepository(): LocationRepository

    fun categoryRepository(): CategoryRepository

    fun foodRepository(): FoodRepository

    fun historyRepository(): HistoryRepository

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): DataComponent
    }
}
