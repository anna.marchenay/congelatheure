package com.marchenaya.data.di.module

import android.content.Context
import com.marchenaya.data.BuildConfig
import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceComponentImpl
import com.marchenaya.data.manager.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
object DataModule {

    @Provides
    @Singleton
    fun appDatabase(context: Context): AppDatabase = AppDatabase.getDatabase(context)

    @Provides
    @Reusable
    fun traceComponent(): TraceComponent = TraceComponentImpl(BuildConfig.DEBUG)
}
