package com.marchenaya.data.manager.db.converter

import androidx.room.TypeConverter
import java.util.Date

class FoodConverter {

    @TypeConverter
    fun fromTimestamp(value: Long): Date? =
        if (value == 0L) null else Date(value)

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long =
        date?.time ?: 0
}
