package com.marchenaya.data.manager.db

import com.marchenaya.data.entity.db.CategoryDBEntity
import com.marchenaya.data.entity.db.FoodDBEntity
import com.marchenaya.data.entity.db.HistoryDBEntity
import com.marchenaya.data.entity.db.LocationDBEntity

interface DBManager {

    fun getLocationList(): List<LocationDBEntity>
    fun saveLocation(locationDBEntity: LocationDBEntity): Long
    fun updateLocation(locationDBEntity: LocationDBEntity)
    fun removeLocationsByIds(locationIds: List<Int>)
    fun getLocationById(locationId: Int): LocationDBEntity
    fun getLocationListById(ids: List<Int>): List<LocationDBEntity>

    fun getCategoryList(): List<CategoryDBEntity>
    fun saveCategory(categoryDBEntity: CategoryDBEntity): Long
    fun updateCategory(categoryDBEntity: CategoryDBEntity)
    fun removeCategoriesByIds(categoryIds: List<Int>)
    fun getCategoryById(categoryId: Int): CategoryDBEntity
    fun getCategoryListById(ids: List<Int>): List<CategoryDBEntity>

    fun getFoodList(): List<FoodDBEntity>
    fun getFood(id: Int): FoodDBEntity
    fun searchFood(name: String): List<FoodDBEntity>
    fun saveFood(foodDBEntity: FoodDBEntity): Long
    fun updateFood(foodDBEntity: FoodDBEntity)
    fun removeFoodsByIds(foodIds: List<Int>)
    fun updateFoodsToOtherCategory(categories: List<Int>)
    fun updateFoodsToUndefinedLocation(locations: List<Int>)
    fun getFoodListById(ids: List<Int>): List<FoodDBEntity>
    fun getFoodListByCategoryIds(categories: List<Int>): List<FoodDBEntity>

    fun getHistoryList(): List<HistoryDBEntity>
    fun searchHistory(text: String): List<HistoryDBEntity>
    fun saveHistory(historyDBEntity: HistoryDBEntity)
}
