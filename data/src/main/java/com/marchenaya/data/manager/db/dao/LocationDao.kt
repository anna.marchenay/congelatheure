package com.marchenaya.data.manager.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.marchenaya.data.entity.db.LocationDBEntity

@Dao
interface LocationDao {

    @Query("SELECT * FROM LocationDBEntity")
    fun getAll(): List<LocationDBEntity>

    @Query("SELECT * FROM LocationDBEntity WHERE id = :locationId")
    fun getById(locationId: Int): LocationDBEntity

    @Query("SELECT * FROM LocationDBEntity WHERE id IN (:locationIds)")
    fun getListById(locationIds: List<Int>): List<LocationDBEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(locationDBEntity: LocationDBEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(locationDBEntity: LocationDBEntity)

    @Query("DELETE FROM LocationDBEntity WHERE id IN (:locationIds)")
    fun removeByIds(locationIds: List<Int>)
}
