package com.marchenaya.data.manager.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.marchenaya.data.entity.db.FoodDBEntity

@Dao
interface FoodDao {

    @Query("SELECT * FROM FoodDBEntity")
    fun getAll(): List<FoodDBEntity>

    @Query("SELECT * FROM FoodDBEntity WHERE id = :id")
    fun get(id: Int): FoodDBEntity

    @Query("SELECT * FROM FoodDBEntity WHERE id IN (:foodIds)")
    fun getListById(foodIds: List<Int>): List<FoodDBEntity>

    @Query("SELECT * FROM FoodDBEntity WHERE name LIKE (:name)")
    fun search(name: String): List<FoodDBEntity>

    @Query("SELECT * FROM FoodDBEntity WHERE categoryId IN (:categories)")
    fun getListByCategoryIds(categories: List<Int>): List<FoodDBEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(foodDBEntity: FoodDBEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(foodDBEntity: FoodDBEntity)

    @Query("DELETE FROM FoodDBEntity WHERE id IN (:foodIds)")
    fun removeByIds(foodIds: List<Int>)

    @Query("UPDATE FoodDBEntity SET categoryId = 1 WHERE categoryId IN (:categories)")
    fun updateFoodsToOtherCategory(categories: List<Int>)

    @Query("UPDATE FoodDBEntity SET locationId = 1 WHERE locationId IN (:locations)")
    fun updateFoodsToUndefinedLocation(locations: List<Int>)
}
