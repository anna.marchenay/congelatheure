package com.marchenaya.data.manager.db

import com.marchenaya.data.entity.db.CategoryDBEntity
import com.marchenaya.data.entity.db.FoodDBEntity
import com.marchenaya.data.entity.db.HistoryDBEntity
import com.marchenaya.data.entity.db.LocationDBEntity
import javax.inject.Inject

class DBManagerImpl @Inject constructor(private val database: AppDatabase) : DBManager {

    override fun getLocationList(): List<LocationDBEntity> = database.locationDao().getAll()

    override fun saveLocation(locationDBEntity: LocationDBEntity): Long =
        database.locationDao().save(locationDBEntity)

    override fun updateLocation(locationDBEntity: LocationDBEntity) =
        database.locationDao().update(locationDBEntity)

    override fun removeLocationsByIds(locationIds: List<Int>) =
        database.locationDao().removeByIds(locationIds)

    override fun getLocationById(locationId: Int): LocationDBEntity =
        database.locationDao().getById(locationId)

    override fun getLocationListById(ids: List<Int>): List<LocationDBEntity> =
        database.locationDao().getListById(ids)

    override fun getCategoryList(): List<CategoryDBEntity> = database.categoryDao().getAll()

    override fun saveCategory(categoryDBEntity: CategoryDBEntity): Long =
        database.categoryDao().save(categoryDBEntity)

    override fun updateCategory(categoryDBEntity: CategoryDBEntity) =
        database.categoryDao().update(categoryDBEntity)

    override fun removeCategoriesByIds(categoryIds: List<Int>) =
        database.categoryDao().removeByIds(categoryIds)

    override fun getCategoryById(categoryId: Int): CategoryDBEntity =
        database.categoryDao().getById(categoryId)

    override fun getCategoryListById(ids: List<Int>): List<CategoryDBEntity> =
        database.categoryDao().getListById(ids)

    override fun getFoodList(): List<FoodDBEntity> = database.foodDao().getAll()

    override fun getFood(id: Int): FoodDBEntity = database.foodDao().get(id)

    override fun searchFood(name: String): List<FoodDBEntity> = database.foodDao().search(name)

    override fun saveFood(foodDBEntity: FoodDBEntity): Long = database.foodDao().save(foodDBEntity)

    override fun updateFood(foodDBEntity: FoodDBEntity) = database.foodDao().update(foodDBEntity)

    override fun removeFoodsByIds(foodIds: List<Int>) = database.foodDao().removeByIds(foodIds)

    override fun updateFoodsToOtherCategory(categories: List<Int>) =
        database.foodDao().updateFoodsToOtherCategory(categories)

    override fun updateFoodsToUndefinedLocation(locations: List<Int>) =
        database.foodDao().updateFoodsToUndefinedLocation(locations)

    override fun getFoodListById(ids: List<Int>): List<FoodDBEntity> =
        database.foodDao().getListById(ids)

    override fun getFoodListByCategoryIds(categories: List<Int>): List<FoodDBEntity> =
        database.foodDao().getListByCategoryIds(categories)

    override fun getHistoryList(): List<HistoryDBEntity> = database.historyDao().getAll()

    override fun searchHistory(text: String): List<HistoryDBEntity> =
        database.historyDao().search(text)

    override fun saveHistory(historyDBEntity: HistoryDBEntity) =
        database.historyDao().save(historyDBEntity)
}
