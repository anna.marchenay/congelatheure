package com.marchenaya.data.manager.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.marchenaya.data.R
import com.marchenaya.data.entity.db.CategoryDBEntity
import com.marchenaya.data.entity.db.FoodDBEntity
import com.marchenaya.data.entity.db.HistoryDBEntity
import com.marchenaya.data.entity.db.LocationDBEntity
import com.marchenaya.data.manager.db.converter.FoodConverter
import com.marchenaya.data.manager.db.dao.CategoryDao
import com.marchenaya.data.manager.db.dao.FoodDao
import com.marchenaya.data.manager.db.dao.HistoryDao
import com.marchenaya.data.manager.db.dao.LocationDao

@Database(
    entities = [
        LocationDBEntity::class,
        CategoryDBEntity::class,
        FoodDBEntity::class,
        HistoryDBEntity::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(FoodConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun locationDao(): LocationDao

    abstract fun categoryDao(): CategoryDao

    abstract fun foodDao(): FoodDao

    abstract fun historyDao(): HistoryDao

    companion object {
        private const val DATABASE_NAME = "congelatheure_database"

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    DATABASE_NAME
                ).addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        db.execSQL(
                            "INSERT INTO LocationDBEntity (name) VALUES ('${
                                context.getString(
                                    R.string.database_undefined
                                )
                            }')"
                        )
                        db.execSQL(
                            "INSERT INTO CategoryDBEntity (name) VALUES ('${
                                context.getString(
                                    R.string.database_other
                                )
                            }'),('${context.getString(R.string.database_meat)}'),('${
                                context.getString(
                                    R.string.database_vegetable
                                )
                            }'),('${context.getString(R.string.database_dessert)}'),('${
                                context.getString(
                                    R.string.database_fish
                                )
                            }'),('${context.getString(R.string.database_pastry)}'),('${
                                context.getString(
                                    R.string.database_fruit
                                )
                            }'),('${context.getString(R.string.database_mushroom)}'),('${
                                context.getString(
                                    R.string.database_cheese
                                )
                            }')"
                        )
                    }
                })
                    .build()
            }.also { INSTANCE = it }
        }
    }
}
