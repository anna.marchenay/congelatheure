package com.marchenaya.data.manager.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.marchenaya.data.entity.db.HistoryDBEntity

@Dao
interface HistoryDao {

    @Query("SELECT * FROM HistoryDBEntity")
    fun getAll(): List<HistoryDBEntity>

    @Query("SELECT * FROM HistoryDBEntity WHERE oldObject LIKE (:text) OR newObject LIKE(:text) OR `replace`(strftime('%d/%m/%Y %H:%M:%S ', datetime(date/1000, 'unixepoch')),'/20','/') LIKE (:text)")
    fun search(text: String): List<HistoryDBEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(historyDBEntity: HistoryDBEntity)
}
