package com.marchenaya.data.manager.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.marchenaya.data.entity.db.CategoryDBEntity

@Dao
interface CategoryDao {

    @Query("SELECT * FROM CategoryDBEntity")
    fun getAll(): List<CategoryDBEntity>

    @Query("SELECT * FROM CategoryDBEntity WHERE id = :categoryId")
    fun getById(categoryId: Int): CategoryDBEntity

    @Query("SELECT * FROM CategoryDBEntity WHERE id IN (:categoryIds)")
    fun getListById(categoryIds: List<Int>): List<CategoryDBEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(categoryDBEntity: CategoryDBEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(categoryDBEntity: CategoryDBEntity)

    @Query("DELETE FROM CategoryDBEntity WHERE id IN (:categoryIds)")
    fun removeByIds(categoryIds: List<Int>)
}
