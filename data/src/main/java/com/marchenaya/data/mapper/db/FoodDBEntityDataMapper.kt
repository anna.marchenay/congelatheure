package com.marchenaya.data.mapper.db

import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceId
import com.marchenaya.data.entity.db.FoodDBEntity
import com.marchenaya.data.entity.local.CategoryEntity
import com.marchenaya.data.entity.local.FoodEntity
import com.marchenaya.data.entity.local.LocationEntity
import com.marchenaya.data.mapper.base.DBMapper
import dagger.Reusable
import javax.inject.Inject

@Reusable
class FoodDBEntityDataMapper @Inject constructor(
    private val traceComponent: TraceComponent
) :
    DBMapper<FoodDBEntity, FoodEntity>() {
    override fun transformEntityToDB(input: FoodEntity): FoodDBEntity = FoodDBEntity(
        input.id,
        input.name,
        input.limitDate,
        input.addDate,
        input.modifiedDate,
        input.imagePath,
        input.quantity,
        input.locationEntity.id,
        input.categoryEntity.id
    )

    override fun transformDBToEntity(input: FoodDBEntity): FoodEntity = FoodEntity(
        input.id,
        input.name,
        input.limitDate,
        input.addDate,
        input.modifiedDate,
        input.imagePath,
        input.quantity,
        LocationEntity(input.locationId, ""),
        CategoryEntity(input.categoryId, "")
    )

    override fun onMappingError(error: Exception) {
        traceComponent.traceError(TraceId.DB_MAPPER_FOOD, "error", error)
    }
}
