package com.marchenaya.data.mapper.local

import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceId
import com.marchenaya.data.entity.local.LocationEntity
import com.marchenaya.data.mapper.base.ModelMapper
import com.marchenaya.data.model.Location
import dagger.Reusable
import javax.inject.Inject

@Reusable
class LocationEntityDataMapper @Inject constructor(private val traceComponent: TraceComponent) :
    ModelMapper<Location, LocationEntity>() {

    override fun transformEntityToModel(input: LocationEntity): Location =
        Location(input.id, input.name)

    override fun transformModelToEntity(input: Location): LocationEntity =
        LocationEntity(input.id, input.name)

    override fun onMappingError(error: Exception) {
        traceComponent.traceError(TraceId.MODEL_MAPPER_LOCATION, "error", error)
    }
}
