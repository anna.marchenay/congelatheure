package com.marchenaya.data.mapper.local

import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceId
import com.marchenaya.data.entity.local.FoodEntity
import com.marchenaya.data.mapper.base.ModelMapper
import com.marchenaya.data.model.Food
import dagger.Reusable
import javax.inject.Inject

@Reusable
class FoodEntityDataMapper @Inject constructor(
    private val traceComponent: TraceComponent,
    private val locationEntityDataMapper: LocationEntityDataMapper,
    private val categoryEntityDataMapper: CategoryEntityDataMapper
) :
    ModelMapper<Food, FoodEntity>() {
    override fun transformEntityToModel(input: FoodEntity): Food = Food(
        input.id,
        input.name,
        input.limitDate,
        input.addDate,
        input.modifiedDate,
        input.imagePath,
        input.quantity,
        locationEntityDataMapper.transformEntityToModel(input.locationEntity),
        categoryEntityDataMapper.transformEntityToModel(input.categoryEntity)
    )

    override fun transformModelToEntity(input: Food): FoodEntity = FoodEntity(
        input.id,
        input.name,
        input.limitDate,
        input.addDate,
        input.modifiedDate,
        input.imagePath,
        input.quantity,
        locationEntityDataMapper.transformModelToEntity(input.location),
        categoryEntityDataMapper.transformModelToEntity(input.category)
    )

    override fun onMappingError(error: Exception) {
        traceComponent.traceError(TraceId.MODEL_MAPPER_FOOD, "error", error)
    }
}
