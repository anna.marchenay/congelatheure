package com.marchenaya.data.mapper.local

import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceId
import com.marchenaya.data.entity.local.CategoryEntity
import com.marchenaya.data.mapper.base.ModelMapper
import com.marchenaya.data.model.Category
import dagger.Reusable
import javax.inject.Inject

@Reusable
class CategoryEntityDataMapper @Inject constructor(private val traceComponent: TraceComponent) :
    ModelMapper<Category, CategoryEntity>() {

    override fun transformEntityToModel(input: CategoryEntity): Category =
        Category(input.id, input.name)

    override fun transformModelToEntity(input: Category): CategoryEntity =
        CategoryEntity(input.id, input.name)

    override fun onMappingError(error: Exception) {
        traceComponent.traceError(TraceId.MODEL_MAPPER_CATEGORY, "error", error)
    }
}
