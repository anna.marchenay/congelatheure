package com.marchenaya.data.mapper.db

import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceId
import com.marchenaya.data.entity.db.HistoryDBEntity
import com.marchenaya.data.entity.local.HistoryEntity
import com.marchenaya.data.mapper.base.DBMapper
import dagger.Reusable
import javax.inject.Inject

@Reusable
class HistoryDBEntityDataMapper @Inject constructor(private val traceComponent: TraceComponent) :
    DBMapper<HistoryDBEntity, HistoryEntity>() {

    override fun transformEntityToDB(input: HistoryEntity): HistoryDBEntity =
        HistoryDBEntity(
            input.id,
            input.type,
            input.oldObject,
            input.newObject,
            input.date
        )

    override fun transformDBToEntity(input: HistoryDBEntity): HistoryEntity = HistoryEntity(
        input.id,
        input.type,
        input.oldObject,
        input.newObject,
        input.date
    )

    override fun onMappingError(error: Exception) {
        traceComponent.traceError(TraceId.DB_MAPPER_HISTORY, "error", error)
    }
}
