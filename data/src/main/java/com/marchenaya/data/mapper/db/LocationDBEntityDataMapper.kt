package com.marchenaya.data.mapper.db

import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceId
import com.marchenaya.data.entity.db.LocationDBEntity
import com.marchenaya.data.entity.local.LocationEntity
import com.marchenaya.data.mapper.base.DBMapper
import dagger.Reusable
import javax.inject.Inject

@Reusable
class LocationDBEntityDataMapper @Inject constructor(private val traceComponent: TraceComponent) :
    DBMapper<LocationDBEntity, LocationEntity>() {

    override fun transformEntityToDB(input: LocationEntity): LocationDBEntity = LocationDBEntity(
        input.id,
        input.name
    )

    override fun transformDBToEntity(input: LocationDBEntity): LocationEntity = LocationEntity(
        input.id,
        input.name
    )

    override fun onMappingError(error: Exception) {
        traceComponent.traceError(TraceId.DB_MAPPER_LOCATION, "error", error)
    }
}
