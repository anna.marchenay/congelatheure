package com.marchenaya.data.mapper.base

abstract class DBMapper<K : Any, T : Any> {

    fun transformDBEntityList(input: List<K>): List<T> {
        return input.mapNotNull {
            try {
                transformDBToEntity(it)
            } catch (e: Exception) {
                onMappingError(e)
                null
            }
        }
    }

    abstract fun transformEntityToDB(input: T): K

    abstract fun transformDBToEntity(input: K): T

    abstract fun onMappingError(error: Exception)
}
