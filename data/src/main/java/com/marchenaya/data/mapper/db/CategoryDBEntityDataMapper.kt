package com.marchenaya.data.mapper.db

import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceId
import com.marchenaya.data.entity.db.CategoryDBEntity
import com.marchenaya.data.entity.local.CategoryEntity
import com.marchenaya.data.mapper.base.DBMapper
import dagger.Reusable
import javax.inject.Inject

@Reusable
class CategoryDBEntityDataMapper @Inject constructor(private val traceComponent: TraceComponent) :
    DBMapper<CategoryDBEntity, CategoryEntity>() {

    override fun transformEntityToDB(input: CategoryEntity): CategoryDBEntity =
        CategoryDBEntity(input.id, input.name)

    override fun transformDBToEntity(input: CategoryDBEntity): CategoryEntity =
        CategoryEntity(input.id, input.name)

    override fun onMappingError(error: Exception) {
        traceComponent.traceError(TraceId.DB_MAPPER_CATEGORY, "error", error)
    }
}
