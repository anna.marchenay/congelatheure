package com.marchenaya.data.mapper.local

import com.marchenaya.data.component.trace.TraceComponent
import com.marchenaya.data.component.trace.TraceId
import com.marchenaya.data.entity.local.HistoryEntity
import com.marchenaya.data.mapper.base.ModelMapper
import com.marchenaya.data.model.History
import dagger.Reusable
import javax.inject.Inject

@Reusable
class HistoryEntityDataMapper @Inject constructor(private val traceComponent: TraceComponent) :
    ModelMapper<History, HistoryEntity>() {

    override fun transformEntityToModel(input: HistoryEntity): History = History(
        input.id,
        input.type,
        input.oldObject,
        input.newObject,
        input.date
    )

    override fun transformModelToEntity(input: History): HistoryEntity = HistoryEntity(
        input.id,
        input.type,
        input.oldObject,
        input.newObject,
        input.date
    )

    override fun onMappingError(error: Exception) {
        traceComponent.traceError(TraceId.MODEL_MAPPER_HISTORY, "error", error)
    }
}
