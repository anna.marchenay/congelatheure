package com.marchenaya.data.entity.local

import com.marchenaya.data.model.Type
import java.util.Date

data class HistoryEntity(
    val id: Int,
    val type: Type,
    val oldObject: String?,
    val newObject: String?,
    val date: Date
)
