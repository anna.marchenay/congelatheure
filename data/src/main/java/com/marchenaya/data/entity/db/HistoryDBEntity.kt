package com.marchenaya.data.entity.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.marchenaya.data.model.Type
import java.util.Date

@Entity
data class HistoryDBEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val type: Type,
    val oldObject: String? = null,
    val newObject: String? = null,
    val date: Date
)
