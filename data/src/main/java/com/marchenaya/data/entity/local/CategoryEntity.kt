package com.marchenaya.data.entity.local

data class CategoryEntity(
    val id: Int,
    val name: String
)
