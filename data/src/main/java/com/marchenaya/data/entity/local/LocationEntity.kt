package com.marchenaya.data.entity.local

data class LocationEntity(
    val id: Int,
    val name: String
)
