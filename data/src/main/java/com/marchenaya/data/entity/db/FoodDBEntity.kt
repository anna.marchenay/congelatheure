package com.marchenaya.data.entity.db

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.Date

@Entity(
    indices = [Index(value = ["locationId"]), Index(value = ["categoryId"])],
    foreignKeys = [
        ForeignKey(
            entity = LocationDBEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("locationId"),
            onDelete = ForeignKey.NO_ACTION
        ),
        ForeignKey(
            entity = CategoryDBEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("categoryId"),
            onDelete = ForeignKey.NO_ACTION
        )
    ]
)
data class FoodDBEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val name: String,
    val limitDate: Date?,
    val addDate: Date,
    val modifiedDate: Date,
    val imagePath: String,
    val quantity: Int,
    val locationId: Int,
    val categoryId: Int
)
