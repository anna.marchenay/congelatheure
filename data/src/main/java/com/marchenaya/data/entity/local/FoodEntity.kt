package com.marchenaya.data.entity.local

import java.util.Date

data class FoodEntity(
    val id: Int,
    val name: String,
    val limitDate: Date?,
    val addDate: Date,
    val modifiedDate: Date,
    val imagePath: String,
    val quantity: Int,
    val locationEntity: LocationEntity,
    val categoryEntity: CategoryEntity
)
