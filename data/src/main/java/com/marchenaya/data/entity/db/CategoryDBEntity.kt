package com.marchenaya.data.entity.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CategoryDBEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val name: String
)
