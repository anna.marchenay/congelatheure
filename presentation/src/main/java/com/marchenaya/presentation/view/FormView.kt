package com.marchenaya.presentation.view

import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.bumptech.glide.signature.ObjectKey
import com.marchenaya.data.model.Category
import com.marchenaya.data.model.Location
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.datawrapper.BaseDataWrapper
import com.marchenaya.presentation.component.dialog.DialogComponent
import com.marchenaya.presentation.component.file.FileComponent
import com.marchenaya.presentation.component.intent.IntentComponent
import com.marchenaya.presentation.databinding.FormBinding
import com.marchenaya.presentation.extensions.convertToString
import com.marchenaya.presentation.extensions.hide
import com.marchenaya.presentation.extensions.show
import com.marchenaya.presentation.utils.FieldValidatorUtils
import com.marchenaya.presentation.utils.TextWatcherUtils.getTextWatcher
import com.marchenaya.presentation.wrapper.CategoryDataWrapper
import com.marchenaya.presentation.wrapper.FoodDataWrapper
import com.marchenaya.presentation.wrapper.LocationDataWrapper
import java.lang.String.CASE_INSENSITIVE_ORDER
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class FormView @kotlin.jvm.JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var binding: FormBinding = FormBinding.inflate(LayoutInflater.from(context), this, true)

    private var currentPhotoUri = Uri.EMPTY
    private var oldFoodDataWrapper: FoodDataWrapper? = null
    private var imageChanged = false
    private var nameChanged = false
    private var limitDateChanged = false
    private var categoryChanged = false
    private var locationChanged = false
    private var quantityChanged = false

    private var locationDataWrapper = LocationDataWrapper(Location(1, ""))
    private var categoryDataWrapper = CategoryDataWrapper(Category(1, ""))

    private lateinit var fileComponent: FileComponent
    private lateinit var dialogComponent: DialogComponent
    private lateinit var intentComponent: IntentComponent
    private lateinit var fieldValidatorUtils: FieldValidatorUtils
    private lateinit var saveFoodFunction: (String, Date?, Int, Uri, LocationDataWrapper, CategoryDataWrapper) -> Unit

    init {
        setupImageClickListeners()
        setupLimitDateClickListener()
        setupSpinnerClickListeners()
        setupSaveClickListener()
    }

    fun setupAddForm(
        fileComponent: FileComponent,
        dialogComponent: DialogComponent,
        intentComponent: IntentComponent,
        fieldValidatorUtils: FieldValidatorUtils,
        saveFoodFunction: (String, Date?, Int, Uri, LocationDataWrapper, CategoryDataWrapper) -> Unit
    ) {
        this.fileComponent = fileComponent
        this.dialogComponent = dialogComponent
        this.intentComponent = intentComponent
        this.fieldValidatorUtils = fieldValidatorUtils
        this.saveFoodFunction = saveFoodFunction
        binding.formButtonSave.isEnabled = true
    }

    fun setupUpdateForm(
        fileComponent: FileComponent,
        dialogComponent: DialogComponent,
        intentComponent: IntentComponent,
        fieldValidatorUtils: FieldValidatorUtils,
        foodDataWrapper: FoodDataWrapper,
        saveFoodFunction: (String, Date?, Int, Uri, LocationDataWrapper, CategoryDataWrapper) -> Unit
    ) {
        this.fileComponent = fileComponent
        this.dialogComponent = dialogComponent
        this.intentComponent = intentComponent
        this.fieldValidatorUtils = fieldValidatorUtils
        this.saveFoodFunction = saveFoodFunction

        fillUpdateForm(foodDataWrapper)
        setupEnableSaveListener()
    }

    private fun fillUpdateForm(
        foodDataWrapper: FoodDataWrapper
    ) {
        oldFoodDataWrapper = foodDataWrapper
        with(binding) {
            currentPhotoUri = foodDataWrapper.getImagePath().toUri()
            Glide.with(context)
                .load(currentPhotoUri).circleCrop()
                .placeholder(R.drawable.ic_image)
                .into(formImage)
            showModifyMode()
            formNameEditText.setText(foodDataWrapper.getName())
            formLimitDatePickerEditText.setText(
                foodDataWrapper.getLimitDate()
                    ?.convertToString(context.getString(R.string.limit_date_format))
            )
            formCategorySpinnerText.setText(foodDataWrapper.getCategoryName(), false)
            categoryDataWrapper = foodDataWrapper.getCategoryDataWrapper()
            formLocationSpinnerText.setText(foodDataWrapper.getLocationName(), false)
            locationDataWrapper = foodDataWrapper.getLocationDataWrapper()
            formNumberPicker.value = foodDataWrapper.getQuantity()
        }
    }

    private fun showModifyMode() {
        with(binding) {
            formButtonModifyImage.show()
            formButtonClearImage.show()
            formImage.show()
            formButtonAddImage.hide()
        }
    }

    fun loadImage(uri: Uri) {
        currentPhotoUri = uri
        Glide.with(this)
            .load(uri).circleCrop()
            .signature(ObjectKey(System.currentTimeMillis().toString()))
            .placeholder(R.drawable.ic_image)
            .into(binding.formImage)
        showModifyMode()
        imageChanged = oldFoodDataWrapper?.getImagePath() != currentPhotoUri.toString()
        enableSave()
    }

    fun setupCategorySpinner(items: List<BaseDataWrapper>) {
        val adapter =
            ArrayAdapter(
                context, R.layout.spinner_list_item,
                items.sortedWith(compareBy(CASE_INSENSITIVE_ORDER) { it.getName() })
            )
        binding.formCategorySpinnerText.setAdapter(adapter)
    }

    fun setupLocationSpinner(items: List<BaseDataWrapper>) {
        val adapter =
            ArrayAdapter(
                context, R.layout.spinner_list_item,
                items.sortedWith(compareBy(CASE_INSENSITIVE_ORDER) { it.getName() })
            )
        binding.formLocationSpinnerText.setAdapter(adapter)
    }

    private fun setupImageClickListeners() {
        with(binding) {
            formButtonAddImage.setOnClickListener { displayChoiceDialog() }

            formButtonModifyImage.setOnClickListener { displayChoiceDialog() }

            formButtonClearImage.setOnClickListener {
                currentPhotoUri = Uri.EMPTY
                showAddMode()
                imageChanged = oldFoodDataWrapper?.getImagePath() != currentPhotoUri.toString()
                enableSave()
            }
        }
    }

    private fun displayChoiceDialog() {
        dialogComponent.displayChoiceDialog(
            context,
            R.string.add_food_image_choice,
            R.array.image_choices_array,
            listOf(
                { intentComponent.galleryIntentResult.launch(intentComponent.galleryIntent) },
                { intentComponent.takePictureIntentResult.launch(intentComponent.takePictureIntent) }
            )
        )
    }

    private fun showAddMode() {
        with(binding) {
            formButtonModifyImage.hide()
            formButtonClearImage.hide()
            formImage.hide()
            formButtonAddImage.show()
        }
    }

    private fun setupLimitDateClickListener() {
        with(binding) {
            formLimitDatePickerEditText.setOnClickListener {
                dialogComponent.displayDateDialog(
                    context,
                    R.string.add_food_date_picker_title,
                ) {
                    val dateString =
                        it.convertToString(context.getString(R.string.limit_date_format))
                    formLimitDatePickerEditText.setText(dateString)
                }
            }
        }
    }

    private fun setupSpinnerClickListeners() {
        with(binding) {
            formCategorySpinnerText.setOnItemClickListener { adapterView, _, i, _ ->
                categoryDataWrapper = (adapterView.getItemAtPosition(i) as CategoryDataWrapper)
                categoryChanged = oldFoodDataWrapper?.getCategoryId() != categoryDataWrapper.getId()
                enableSave()
            }
            formLocationSpinnerText.setOnItemClickListener { adapterView, _, i, _ ->
                locationDataWrapper = (adapterView.getItemAtPosition(i) as LocationDataWrapper)
                locationChanged = oldFoodDataWrapper?.getLocationId() != locationDataWrapper.getId()
                enableSave()
            }
        }
    }

    private fun setupSaveClickListener() {
        with(binding) {
            formButtonSave.setOnClickListener {
                if (fieldValidatorUtils.isFilled(
                        context,
                        formNameEditText
                    ) and fieldValidatorUtils.isFilled(
                        context,
                        formCategorySpinnerText
                    ) and fieldValidatorUtils.isFilled(
                        context,
                        formLocationSpinnerText
                    )
                ) {
                    saveFoodFunction(
                        formNameEditText.text.toString(),
                        if (formLimitDatePickerEditText.text.isNullOrEmpty()) {
                            null
                        } else {
                            SimpleDateFormat(
                                context.getString(R.string.limit_date_format),
                                Locale.getDefault()
                            ).parse(formLimitDatePickerEditText.text.toString())
                        },
                        formNumberPicker.value,
                        currentPhotoUri,
                        locationDataWrapper,
                        categoryDataWrapper
                    )
                }
            }
        }
    }

    private fun setupEnableSaveListener() {
        with(binding) {
            formNameEditText.addTextChangedListener(getTextWatcher {
                nameChanged = oldFoodDataWrapper?.getName() != it
                enableSave()
            })
            formLimitDatePickerEditText.addTextChangedListener(getTextWatcher {
                limitDateChanged = oldFoodDataWrapper?.getLimitDate()
                    ?.convertToString(context.getString(R.string.limit_date_format)) != it
                enableSave()
            })
            formNumberPicker.onTextChangedListener {
                quantityChanged = oldFoodDataWrapper?.getQuantity() != it
                enableSave()
            }
        }
    }

    private fun enableSave() {
        binding.formButtonSave.isEnabled =
            imageChanged || nameChanged || limitDateChanged || categoryChanged ||
                    locationChanged || quantityChanged
    }

}
