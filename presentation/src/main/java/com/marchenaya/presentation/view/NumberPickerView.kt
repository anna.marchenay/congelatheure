package com.marchenaya.presentation.view

import android.content.Context
import android.text.Editable
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.button.MaterialButton
import com.marchenaya.presentation.databinding.NumberPickerBinding
import com.marchenaya.presentation.utils.TextWatcherUtils.getTextWatcher

const val MIN_PICKER = 1
const val MAX_PICKER = 99
const val DELAY = 50L

class NumberPickerView @kotlin.jvm.JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attrs, defStyle) {

    private var binding: NumberPickerBinding =
        NumberPickerBinding.inflate(LayoutInflater.from(context), this, true)

    var value: Int
        get() = binding.numberPickerNumberPickerEditText.text.toString().toInt()
        set(number) {
            binding.numberPickerNumberPickerEditText.text =
                Editable.Factory.getInstance().newEditable(controlValue(number).toString())
        }

    init {
        setOnCLickListeners()
    }

    private fun setOnCLickListeners() {
        with(binding) {
            numberPickerMinusButton.setOnClickListener { value -= 1 }

            numberPickerPlusButton.setOnClickListener { value += 1 }

            numberPickerMinusButton.setOnLongClickListener {
                changeValueDuringLongClick(numberPickerMinusButton) { value = it - 1 }
            }

            numberPickerPlusButton.setOnLongClickListener {
                changeValueDuringLongClick(numberPickerPlusButton) { value = it + 1 }
            }
        }
    }

    private fun controlValue(number: Int): Int {
        return when {
            number < MIN_PICKER -> MIN_PICKER
            number > MAX_PICKER -> MAX_PICKER
            else -> number
        }
    }

    private fun changeValueDuringLongClick(
        button: MaterialButton,
        setValue: (Int) -> Unit
    ): Boolean {
        val runnable: Runnable = object : Runnable {
            override fun run() {
                if (button.isPressed) {
                    setValue(value)
                    handler.postDelayed(this, DELAY)
                }
            }
        }
        handler.postDelayed(runnable, 0)
        return true
    }

    fun onTextChangedListener(onTextChanged: (Int) -> Unit) {
        binding.numberPickerNumberPickerEditText.addTextChangedListener(getTextWatcher {
            onTextChanged(it.toInt())
        })
    }
}
