package com.marchenaya.presentation.utils

import com.google.gson.Gson
import com.marchenaya.data.model.Category
import com.marchenaya.data.model.Food
import com.marchenaya.data.model.HistoryDescription
import com.marchenaya.data.model.Location
import com.marchenaya.data.model.Type
import com.marchenaya.presentation.extensions.convertToString

object HistoryDescriptionUtils {

    fun getHistoryDescription(
        historyDescription: HistoryDescription,
        type: Type,
        oldObject: String?,
        newObject: String?,
    ): String = when (type) {
        Type.CATEGORY -> {
            generateCategoryDescription(
                Gson().fromJson(
                    oldObject,
                    Category::class.java
                ), Gson().fromJson(
                    newObject,
                    Category::class.java
                ), historyDescription
            )
        }
        Type.LOCATION -> {
            generateLocationDescription(
                Gson().fromJson(
                    oldObject,
                    Location::class.java
                ), Gson().fromJson(
                    newObject,
                    Location::class.java
                ), historyDescription
            )
        }
        Type.FOOD -> {
            generateFoodDescription(
                Gson().fromJson(
                    oldObject,
                    Food::class.java
                ), Gson().fromJson(
                    newObject,
                    Food::class.java
                ), historyDescription
            )
        }
    }

    private fun generateCategoryDescription(
        oldCategory: Category?,
        newCategory: Category?,
        historyDescription: HistoryDescription,
    ): String = when {
        oldCategory != null && newCategory != null -> {
            String.format(
                historyDescription.historyCategoryModified,
                oldCategory.name,
                newCategory.name
            )
        }
        oldCategory != null && newCategory == null -> {
            String.format(historyDescription.historyCategoryRemoved, oldCategory.name)
        }
        oldCategory == null && newCategory != null -> {
            String.format(historyDescription.historyCategoryAdded, newCategory.name)
        }
        else -> {
            ""
        }
    }

    private fun generateLocationDescription(
        oldLocation: Location?,
        newLocation: Location?,
        historyDescription: HistoryDescription,
    ): String = when {
        oldLocation != null && newLocation != null -> {
            String.format(
                historyDescription.historyLocationModified,
                oldLocation.name,
                newLocation.name
            )
        }
        oldLocation != null && newLocation == null -> {
            String.format(historyDescription.historyLocationRemoved, oldLocation.name)
        }
        oldLocation == null && newLocation != null -> {
            String.format(historyDescription.historyLocationAdded, newLocation.name)
        }
        else -> {
            ""
        }
    }

    private fun generateFoodDescription(
        oldFood: Food?,
        newFood: Food?,
        historyDescription: HistoryDescription,
    ): String = when {
        oldFood != null && newFood != null -> {
            getFoodModificationDescription(oldFood, newFood, historyDescription)
        }
        oldFood != null && newFood == null -> {
            String.format(historyDescription.historyLocationRemoved, oldFood.name)
        }
        oldFood == null && newFood != null -> {
            String.format(historyDescription.historyFoodAdded, newFood.name)
        }
        else -> {
            ""
        }
    }

    private fun getFoodModificationDescription(
        oldFood: Food,
        newFood: Food,
        historyDescription: HistoryDescription,
    ): String {
        var description = String.format(historyDescription.historyFoodModified, oldFood.name)

        if (oldFood.name != newFood.name) {
            description += String.format(
                historyDescription.historyFoodModifiedName,
                oldFood.name,
                newFood.name
            )
        }
        if (oldFood.limitDate != newFood.limitDate && oldFood.limitDate != null && newFood.limitDate != null) {
            description += String.format(
                historyDescription.historyFoodModifiedLimitDate,
                oldFood.limitDate!!.convertToString(historyDescription.limitDateFormat),
                newFood.limitDate!!.convertToString(historyDescription.limitDateFormat)
            )
        } else if (newFood.limitDate == null && oldFood.limitDate != null) {
            description += String.format(
                historyDescription.historyFoodModifiedNullLimitDate,
                oldFood.limitDate!!.convertToString(historyDescription.limitDateFormat)
            )
        }
        if (oldFood.imagePath != newFood.imagePath && oldFood.imagePath.isNotEmpty()) {
            description += String.format(historyDescription.historyFoodModifiedImage)
        }
        if (oldFood.quantity != newFood.quantity) {
            description += String.format(
                historyDescription.historyFoodModifiedQuantity,
                oldFood.quantity.toString(),
                newFood.quantity.toString()
            )
        }
        if (oldFood.category.id != newFood.category.id && newFood.category.name.isNotEmpty()) {
            description +=
                String.format(
                    historyDescription.historyFoodModifiedCategory,
                    oldFood.category.name,
                    newFood.category.name
                )
        }
        if (oldFood.location.id != newFood.location.id && newFood.location.name.isNotEmpty()) {
            description +=
                String.format(
                    historyDescription.historyFoodModifiedLocation,
                    oldFood.location.name,
                    newFood.location.name
                )
        }
        return description
    }
}
