package com.marchenaya.presentation.utils

import android.content.Context
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import com.marchenaya.data.exception.InputLayoutException
import com.marchenaya.presentation.R
import dagger.Reusable
import javax.inject.Inject

private const val NO_ERROR = ""

@Reusable
class FieldValidatorUtils @Inject constructor() {

    fun isFilled(context: Context, editText: EditText): Boolean {
        val parent = editText.getTextInputLayout(context)
        return if (editText.text.isNullOrEmpty()) {
            parent.error = context.getString(R.string.general_field_validator_mandatory_error)
            false
        } else {
            parent.error = NO_ERROR
            true
        }
    }

    private fun EditText.getTextInputLayout(context: Context): TextInputLayout {
        val textInputLayout = parent.parent
        return if (textInputLayout is TextInputLayout) {
            textInputLayout
        } else {
            throw InputLayoutException(context.getString(R.string.general_field_validator_text_input_parent_error))
        }
    }
}
