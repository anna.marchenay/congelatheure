package com.marchenaya.presentation.utils

import com.marchenaya.data.model.FoodSorter

object GlobalValueHelper {
    var globalFilteredCategoryIdList: List<Int> = listOf()
    var globalFoodSorter: FoodSorter = FoodSorter.NAME_ASC
}
