package com.marchenaya.presentation.wrapper

import com.marchenaya.data.model.Location
import com.marchenaya.presentation.base.datawrapper.BaseDataWrapper

class LocationDataWrapper(private val location: Location) : BaseDataWrapper {

    override fun getId() = location.id

    override fun getName() = location.name

    override fun toString(): String {
        return getName()
    }
}
