package com.marchenaya.presentation.wrapper

import android.content.Context
import com.marchenaya.data.model.History
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.datawrapper.BaseDataWrapper
import com.marchenaya.presentation.extensions.convertToString

class HistoryDataWrapper(private val history: History) : BaseDataWrapper {

    override fun getId() = history.id

    override fun getName() = history.id.toString()

    fun getType() = history.type

    fun getOldObject() = history.oldObject

    fun getNewObject() = history.newObject

    fun getDate() = history.date

    fun getHourStringDate(context: Context) =
        history.date.convertToString(context.getString(R.string.history_hour_format))
}
