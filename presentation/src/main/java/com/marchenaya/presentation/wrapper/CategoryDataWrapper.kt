package com.marchenaya.presentation.wrapper

import com.marchenaya.data.model.Category
import com.marchenaya.presentation.base.datawrapper.BaseDataWrapper

class CategoryDataWrapper(private val category: Category) : BaseDataWrapper {

    override fun getId() = category.id

    override fun getName() = category.name

    override fun toString(): String {
        return getName()
    }
}
