package com.marchenaya.presentation.wrapper

import com.marchenaya.data.model.Food
import com.marchenaya.presentation.base.datawrapper.BaseDataWrapper
import java.util.Date

class FoodDataWrapper(private val food: Food) : BaseDataWrapper {

    override fun getId() = food.id

    override fun getName() = food.name

    fun getLimitDate(): Date? = food.limitDate

    fun getAddDate() = food.addDate

    fun getModifiedDate() = food.modifiedDate

    fun getImagePath() = food.imagePath

    fun getStringQuantity() = food.quantity.toString()

    fun getQuantity() = food.quantity

    fun getLocationDataWrapper() = LocationDataWrapper(food.location)

    fun getLocationId() = food.location.id

    fun getLocationName() = food.location.name

    fun getCategoryDataWrapper() = CategoryDataWrapper(food.category)

    fun getCategoryId() = food.category.id

    fun getCategoryName() = food.category.name

    fun getNameAndQuantity() = "${getName()} (x${getStringQuantity()})"

    override fun toString(): String {
        return getName()
    }
}
