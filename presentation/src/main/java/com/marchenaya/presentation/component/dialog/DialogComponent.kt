package com.marchenaya.presentation.component.dialog

import android.content.Context
import androidx.annotation.StringRes
import java.util.Date

interface DialogComponent {

    fun displayEditTextDialog(
        context: Context,
        @StringRes title: Int,
        @StringRes inputHint: Int,
        @StringRes positiveText: Int,
        isCancelable: Boolean,
        currentValue: String = "",
        onPositiveClick: (String) -> Unit
    )

    fun displayChoiceDialog(
        context: Context,
        title: Int,
        choicesArray: Int,
        choicesEventList: List<() -> Unit>
    )

    fun displayDateDialog(
        context: Context,
        @StringRes title: Int,
        onValidateClick: (Date) -> Unit
    )

    fun displayRemoveWarningDialog(
        context: Context,
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes positiveText: Int,
        @StringRes negativeText: Int,
        onPositiveClick: () -> Unit
    )
}
