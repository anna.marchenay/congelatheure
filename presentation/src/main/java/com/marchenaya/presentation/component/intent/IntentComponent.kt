package com.marchenaya.presentation.component.intent

import android.content.Context
import android.content.Intent
import androidx.activity.result.ActivityResultLauncher
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

interface IntentComponent {

    var galleryIntent: Intent
    var takePictureIntent: Intent
    var galleryIntentResult: ActivityResultLauncher<Intent>
    var takePictureIntentResult: ActivityResultLauncher<Intent>

    fun setupIntents(context: Context, fragment: Fragment, binding: ViewBinding)
}
