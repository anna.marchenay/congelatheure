package com.marchenaya.presentation.component.error

import android.content.Context
import com.marchenaya.data.exception.DatabaseException
import com.marchenaya.data.exception.InputLayoutException
import com.marchenaya.data.exception.ViewModelCreationException
import com.marchenaya.presentation.R
import javax.inject.Inject

class ErrorTranslator @Inject constructor() {

    fun translate(context: Context, throwable: Throwable): String {
        return context.getString(
            when (throwable) {
                is DatabaseException -> R.string.error_database_request_failed
                is InputLayoutException -> R.string.error_input_layout
                is ViewModelCreationException -> R.string.error_view_model_creation
                else -> R.string.error_general
            }
        )
    }
}
