package com.marchenaya.presentation.component.materialcardview

import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.Selection
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.RecyclerView
import com.marchenaya.presentation.R
import com.marchenaya.presentation.ui.categoryList.item.CategoryListFragmentViewHolder
import com.marchenaya.presentation.ui.foodList.item.FoodListFragmentViewHolder
import com.marchenaya.presentation.ui.locationList.item.LocationListFragmentViewHolder
import javax.inject.Inject

private const val SELECTION_ID = "card_selection"

class MaterialCardViewComponentImpl @Inject constructor() : MaterialCardViewComponent {

    private var actionMode: ActionMode? = null
    private lateinit var selectionTracker: SelectionTracker<Long>
    private val actionModeCallback = ActionModeCallback()
    private lateinit var onRemoveButtonClicked: (Selection<Long>) -> Unit
    private lateinit var recyclerView: RecyclerView
    private var displayDialog: ((() -> Unit) -> Unit)? = null

    override fun selectCards(
        activity: AppCompatActivity,
        recyclerView: RecyclerView,
        onRemoveButtonClicked: (Selection<Long>) -> Unit,
        getPosition: (() -> Long)?,
        setSelectionTracker: (SelectionTracker<Long>) -> Unit,
        displayDialog: ((() -> Unit) -> Unit)?
    ) {

        val keyProvider = object :
            ItemKeyProvider<Long>(SCOPE_MAPPED) {
            override fun getKey(position: Int): Long {
                return position.toLong()
            }

            override fun getPosition(key: Long): Int {
                return key.toInt()
            }
        }

        val detailsLookup = object : ItemDetailsLookup<Long>() {
            override fun getItemDetails(e: MotionEvent): ItemDetails<Long>? {
                val view = recyclerView.findChildViewUnder(e.x, e.y)
                if (view != null) {
                    val viewHolder =
                        recyclerView.getChildViewHolder(view)
                    return when (viewHolder) {
                        is LocationListFragmentViewHolder -> viewHolder.getItemDetails()
                        is CategoryListFragmentViewHolder -> viewHolder.getItemDetails()
                        is FoodListFragmentViewHolder -> viewHolder.getItemDetails()
                        else -> null
                    }
                }
                return null
            }
        }

        val selectionObserver = object : SelectionTracker.SelectionObserver<Long?>() {
            override fun onSelectionChanged() {
                if (selectionTracker.selection.size() > 0) {
                    if (actionMode == null) {
                        actionMode =
                            activity.startSupportActionMode(
                                actionModeCallback
                            )
                    }
                    actionMode?.title = selectionTracker.selection.size().toString()
                } else actionMode?.finish()
            }
        }

        this.onRemoveButtonClicked = onRemoveButtonClicked
        this.recyclerView = recyclerView
        this.displayDialog = displayDialog

        selectionTracker = SelectionTracker.Builder(
            SELECTION_ID,
            recyclerView,
            keyProvider,
            detailsLookup,
            StorageStrategy.createLongStorage()
        )
            .withSelectionPredicate(object : SelectionTracker.SelectionPredicate<Long>() {
                override fun canSetStateForKey(key: Long, nextState: Boolean): Boolean =
                    if (getPosition != null) {
                        key != getPosition()
                    } else {
                        true
                    }

                override fun canSetStateAtPosition(position: Int, nextState: Boolean): Boolean {
                    return true
                }

                override fun canSelectMultiple(): Boolean {
                    return true
                }
            })
            .build()

        setSelectionTracker(selectionTracker)

        selectionTracker.addObserver(selectionObserver)
    }

    override fun finishActionMode() {
        actionMode?.finish()
    }

    private inner class ActionModeCallback : ActionMode.Callback {
        override fun onCreateActionMode(p0: ActionMode?, p1: Menu?): Boolean {
            p0?.menuInflater?.inflate(R.menu.delete_menu, p1)
            return true
        }

        override fun onPrepareActionMode(p0: ActionMode?, p1: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(p0: ActionMode?, p1: MenuItem?): Boolean {
            if (displayDialog != null) {
                displayDialog?.invoke {
                    onRemoveButtonClicked(selectionTracker.selection)
                    onDestroyActionMode(p0)
                }
            } else {
                onRemoveButtonClicked(selectionTracker.selection)
                onDestroyActionMode(p0)
            }
            return true
        }

        override fun onDestroyActionMode(p0: ActionMode?) {
            selectionTracker.clearSelection()
            actionMode = null
        }
    }
}
