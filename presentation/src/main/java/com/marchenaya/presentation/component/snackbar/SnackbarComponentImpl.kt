package com.marchenaya.presentation.component.snackbar

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.marchenaya.presentation.R
import com.marchenaya.presentation.component.error.ErrorTranslator
import com.marchenaya.presentation.extensions.getColorFromAttr
import javax.inject.Inject

class SnackbarComponentImpl @Inject constructor(private val errorTranslator: ErrorTranslator) :
    SnackbarComponent {

    override fun displaySuccess(context: Context, content: String, view: View?) {
        view?.let {
            Snackbar.make(it, content, Snackbar.LENGTH_LONG).apply {
                setBackgroundTint(context.getColorFromAttr(R.attr.snackbarSuccessBackground))
                setTextColor(ContextCompat.getColor(context, R.color.white))
                setAnchorView(R.id.activity_main_navigation_bar)
            }.show()
        }
    }

    override fun displayWarning(context: Context, content: String, view: View?) {
        view?.let {
            Snackbar.make(it, content, Snackbar.LENGTH_LONG).apply {
                setBackgroundTint(context.getColorFromAttr(R.attr.snackbarWarningBackground))
                setTextColor(ContextCompat.getColor(context, R.color.white))
                setAnchorView(R.id.activity_main_navigation_bar)
            }.show()
        }
    }

    override fun displayError(context: Context, throwable: Throwable, view: View?) {
        view?.let {
            Snackbar.make(it, errorTranslator.translate(context, throwable), Snackbar.LENGTH_LONG)
                .apply {
                    setBackgroundTint(context.getColorFromAttr(R.attr.snackbarErrorBackground))
                    setTextColor(ContextCompat.getColor(context, R.color.white))
                    setAnchorView(R.id.activity_main_navigation_bar)
                }.show()
        }
    }
}
