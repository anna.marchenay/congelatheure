package com.marchenaya.presentation.component.intent

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.provider.MediaStore
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.marchenaya.presentation.R
import com.marchenaya.presentation.component.file.FileComponent
import com.marchenaya.presentation.databinding.FragmentAddFoodBinding
import com.marchenaya.presentation.databinding.FragmentUpdateFoodBinding
import javax.inject.Inject

class IntentComponentImpl @Inject constructor() : IntentComponent {

    @Inject
    lateinit var fileComponent: FileComponent

    override lateinit var galleryIntent: Intent
    override lateinit var takePictureIntent: Intent
    override lateinit var galleryIntentResult: ActivityResultLauncher<Intent>
    override lateinit var takePictureIntentResult: ActivityResultLauncher<Intent>

    override fun setupIntents(context: Context, fragment: Fragment, binding: ViewBinding) {
        setupGalleryIntent(context, fragment, binding)
        setupTakePictureIntent(context, fragment, binding)
    }

    private fun setupGalleryIntent(context: Context, fragment: Fragment, binding: ViewBinding) {

        galleryIntent =
            Intent(Intent.ACTION_PICK).apply { type = context.getString(R.string.file_type) }

        this.galleryIntentResult = fragment.registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                result.data?.data?.let {
                    when (binding) {
                        is FragmentAddFoodBinding -> {
                            binding.addFoodForm.loadImage(it)
                        }
                        is FragmentUpdateFoodBinding -> {
                            binding.updateFoodForm.loadImage(it)
                        }
                    }
                }
            }
        }
    }

    private fun setupTakePictureIntent(
        context: Context,
        fragment: Fragment,
        binding: ViewBinding
    ) {
        val uri = FileProvider.getUriForFile(
            context,
            context.getString(R.string.authority),
            fileComponent.createImageFile(context)
        )

        takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
            putExtra(MediaStore.EXTRA_OUTPUT, uri)
        }

        this.takePictureIntentResult =
            fragment.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) {
                    when (binding) {
                        is FragmentAddFoodBinding -> {
                            binding.addFoodForm.loadImage(uri)
                        }
                        is FragmentUpdateFoodBinding -> {
                            binding.updateFoodForm.loadImage(uri)
                        }
                    }
                }
            }
    }
}
