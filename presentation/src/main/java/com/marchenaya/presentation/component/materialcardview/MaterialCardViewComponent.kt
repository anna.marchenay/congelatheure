package com.marchenaya.presentation.component.materialcardview

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.selection.Selection
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView

interface MaterialCardViewComponent {

    fun selectCards(
        activity: AppCompatActivity,
        recyclerView: RecyclerView,
        onRemoveButtonClicked: (Selection<Long>) -> Unit,
        getPosition: (() -> Long)? = null,
        setSelectionTracker: (SelectionTracker<Long>) -> Unit,
        displayDialog: ((() -> Unit) -> Unit)? = null
    )

    fun finishActionMode()
}
