package com.marchenaya.presentation.component.file

import android.content.Context
import android.os.Environment
import com.marchenaya.presentation.R
import com.marchenaya.presentation.extensions.convertToString
import java.io.File
import java.util.Date
import javax.inject.Inject

class FileComponentImpl @Inject constructor() : FileComponent {

    override fun createImageFile(context: Context): File {
        val timeStamp = Date().convertToString(context.getString(R.string.file_date_format))
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "${context.getString(R.string.file_prefix)}_${timeStamp}_",
            context.getString(R.string.file_suffix),
            storageDir
        )
    }
}
