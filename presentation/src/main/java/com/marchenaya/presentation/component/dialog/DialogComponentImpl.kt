package com.marchenaya.presentation.component.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import androidx.annotation.StringRes
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.setActionButtonEnabled
import com.afollestad.materialdialogs.datetime.datePicker
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItems
import com.marchenaya.presentation.R
import com.marchenaya.presentation.extensions.getContent
import com.marchenaya.presentation.utils.TextWatcherUtils.getTextWatcher
import java.util.Date
import javax.inject.Inject

class DialogComponentImpl @Inject constructor() :
    DialogComponent {

    private var currentDialog: Dialog? = null

    @SuppressLint("CheckResult")
    override fun displayEditTextDialog(
        context: Context,
        title: Int,
        inputHint: Int,
        positiveText: Int,
        isCancelable: Boolean,
        currentValue: String,
        onPositiveClick: (String) -> Unit
    ) {

        currentDialog = MaterialDialog(context).show {
            cornerRadius(res = R.dimen.base_corner_radius)
            title(title)
            cancelable(isCancelable)
            input(hintRes = inputHint, waitForPositiveButton = false, prefill = currentValue)
            positiveButton(positiveText) {
                onPositiveClick(it.getInputField().getContent())
            }
            setActionButtonEnabled(WhichButton.POSITIVE, false)
            getInputField().addTextChangedListener(getTextWatcher {
                if (currentValue != it) {
                    setActionButtonEnabled(WhichButton.POSITIVE, true)
                } else {
                    setActionButtonEnabled(WhichButton.POSITIVE, false)
                }
            })
        }
    }

    @SuppressLint("CheckResult")
    override fun displayChoiceDialog(
        context: Context,
        title: Int,
        choicesArray: Int,
        choicesEventList: List<() -> Unit>
    ) {
        currentDialog = MaterialDialog(context)
            .show {
                cornerRadius(res = R.dimen.base_corner_radius)
                title(title)
                listItems(choicesArray) { _, index, _ ->
                    choicesEventList[index]()
                }
            }
    }

    override fun displayDateDialog(
        context: Context,
        @StringRes title: Int,
        onValidateClick: (Date) -> Unit
    ) {
        currentDialog = MaterialDialog(context).show {
            title(title)
            datePicker { _, calendar ->
                onValidateClick(calendar.time)
            }
        }
    }

    override fun displayRemoveWarningDialog(
        context: Context,
        title: Int,
        message: Int,
        positiveText: Int,
        negativeText: Int,
        onPositiveClick: () -> Unit
    ) {
        currentDialog = MaterialDialog(context).show {
            title(title)
            message(message)
            positiveButton(positiveText) {
                onPositiveClick()
            }
            negativeButton(negativeText)
        }
    }
}
