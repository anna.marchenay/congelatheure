package com.marchenaya.presentation.component.file

import android.content.Context
import java.io.File

interface FileComponent {

    fun createImageFile(context: Context): File
}
