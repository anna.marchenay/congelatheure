package com.marchenaya.presentation.extensions

import android.content.Context
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import com.google.android.material.card.MaterialCardView
import com.marchenaya.presentation.R
import java.util.Calendar
import java.util.Date
import java.util.concurrent.TimeUnit

const val RED_LIMIT_DATE = 7
const val ORANGE_LIMIT_DATE = 14

fun EditText.getContent() = text.toString()

@ColorInt
fun Context.getColorFromAttr(
    @AttrRes attrColor: Int,
    typedValue: TypedValue = TypedValue(),
    resolveRefs: Boolean = true
): Int {
    theme.resolveAttribute(attrColor, typedValue, resolveRefs)
    return typedValue.data
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun MaterialCardView.setCardBackgroundColorDependingOnLimitDate(limitDate: Date?, itemView: View) {
    val now = Calendar.getInstance().time
    var color = R.color.whiteLimitDate
    if (limitDate != null) {
        val dayDiff = TimeUnit.MILLISECONDS.toDays(limitDate.time - now.time)
        color = when {
            dayDiff < 0 -> {
                R.color.redLimitDate
            }
            dayDiff <= RED_LIMIT_DATE -> {
                R.color.orangeLimitDate
            }
            dayDiff <= ORANGE_LIMIT_DATE -> {
                R.color.yellowLimitDate
            }
            else -> {
                R.color.greenLimitDate
            }
        }
    }
    strokeWidth = resources.getDimensionPixelSize(R.dimen.limit_date_image_stroke_width)
    strokeColor = ContextCompat.getColor(itemView.context, color)
}

fun MenuItem.show() {
    isVisible = true
}

fun MenuItem.hide() {
    isVisible = false
}
