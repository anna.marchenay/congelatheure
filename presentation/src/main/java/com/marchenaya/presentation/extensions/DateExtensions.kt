package com.marchenaya.presentation.extensions

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun Date.convertToString(format: String): String =
    SimpleDateFormat(
        format,
        Locale.getDefault()
    ).format(this)
