package com.marchenaya.presentation.ui.foodDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.marchenaya.data.repository.FoodRepository
import com.marchenaya.presentation.base.SingleLiveEvent
import com.marchenaya.presentation.base.viewmodel.BaseViewModel
import com.marchenaya.presentation.extensions.subscribeByIO
import com.marchenaya.presentation.wrapper.FoodDataWrapper
import timber.log.Timber
import javax.inject.Inject

class FoodDetailFragmentViewModel @Inject constructor(
    private val foodRepository: FoodRepository
) : BaseViewModel() {

    private val foodLiveData = MutableLiveData<FoodDataWrapper>()
    private val removedFoodLiveEvent = SingleLiveEvent<String>()
    private val errorLiveEvent = SingleLiveEvent<Throwable>()

    fun getFoodLiveData(): LiveData<FoodDataWrapper> = foodLiveData
    fun getRemovedFoodLiveEvent(): LiveData<String> = removedFoodLiveEvent

    fun getErrorLiveEvent(): LiveData<Throwable> = errorLiveEvent

    fun retrieveFoodDetail(id: Int) {
        foodRepository.getFood(id).subscribeByIO(
            onSuccess = {
                foodLiveData.postValue(FoodDataWrapper(it))
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun removeFood(foodToDelete: FoodDataWrapper) {
        foodRepository.removeFoods(listOf(foodToDelete.getId()))
            .subscribeByIO(
                onComplete = {
                    Timber.i("Food deleted : ${foodToDelete.getName()}")
                    removedFoodLiveEvent.postValue(foodToDelete.getName())
                },
                onError = {
                    Timber.e(it)
                    errorLiveEvent.postValue(it)
                }
            )
    }
}
