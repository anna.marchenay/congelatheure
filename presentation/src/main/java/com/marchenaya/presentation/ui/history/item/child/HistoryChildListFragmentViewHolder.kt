package com.marchenaya.presentation.ui.history.item.child

import android.content.Context
import com.marchenaya.data.model.HistoryDescription
import com.marchenaya.presentation.R
import com.marchenaya.presentation.databinding.HistoryChildListItemBinding
import com.marchenaya.presentation.utils.HistoryDescriptionUtils.getHistoryDescription
import com.marchenaya.presentation.wrapper.HistoryDataWrapper

class HistoryChildListFragmentViewHolder(
    private val binding: HistoryChildListItemBinding,
    private val context: Context
) {

    fun bind(historyDataWrapper: HistoryDataWrapper) {
        with(binding) {
            val historyDescription = HistoryDescription(
                context.getString(R.string.history_category_modified),
                context.getString(R.string.history_category_removed),
                context.getString(R.string.history_category_added),
                context.getString(R.string.history_location_modified),
                context.getString(R.string.history_location_removed),
                context.getString(R.string.history_location_added),
                context.getString(R.string.history_food_modified),
                context.getString(R.string.history_food_removed),
                context.getString(R.string.history_food_added),
                context.getString(R.string.history_food_modified_name),
                context.getString(R.string.history_food_modified_limit_date),
                context.getString(R.string.history_food_modified_null_limit_date),
                context.getString(R.string.history_food_modified_image),
                context.getString(R.string.history_food_modified_quantity),
                context.getString(R.string.history_food_modified_category),
                context.getString(R.string.history_food_modified_location),
                context.getString(R.string.limit_date_format)
            )
            historyChildHour.text = historyDataWrapper.getHourStringDate(context)
            historyChildText.text = getHistoryDescription(
                historyDescription,
                historyDataWrapper.getType(),
                historyDataWrapper.getOldObject(),
                historyDataWrapper.getNewObject()
            )
        }
    }
}
