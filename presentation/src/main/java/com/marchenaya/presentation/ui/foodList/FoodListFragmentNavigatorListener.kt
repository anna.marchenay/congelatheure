package com.marchenaya.presentation.ui.foodList

interface FoodListFragmentNavigatorListener {

    fun displayAddFoodFragment()

    fun displayFoodDetailFragment(foodId: Int, foodName: String)
}
