package com.marchenaya.presentation.ui.categoryList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.marchenaya.data.repository.CategoryRepository
import com.marchenaya.presentation.base.SingleLiveEvent
import com.marchenaya.presentation.base.viewmodel.BaseViewModel
import com.marchenaya.presentation.extensions.subscribeByIO
import com.marchenaya.presentation.wrapper.CategoryDataWrapper
import javax.inject.Inject
import timber.log.Timber

class CategoryListFragmentViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository
) :
    BaseViewModel() {

    private val categoryListLiveData = MutableLiveData<List<CategoryDataWrapper>>()
    private val savedCategoryLiveEvent = SingleLiveEvent<String>()
    private val updatedCategoryLiveEvent = SingleLiveEvent<Pair<String, String>>()
    private val removedSingleCategoryLiveEvent = SingleLiveEvent<String>()
    private val removedMultipleCategoriesLiveEvent = SingleLiveEvent<String>()
    private val errorLiveEvent = SingleLiveEvent<Throwable>()

    fun getCategoryListLiveData(): LiveData<List<CategoryDataWrapper>> = categoryListLiveData
    fun getSavedCategoryLiveEvent(): LiveData<String> = savedCategoryLiveEvent
    fun getUpdatedCategoryLiveEvent(): LiveData<Pair<String, String>> = updatedCategoryLiveEvent
    fun getRemovedSingleCategoryLiveEvent(): LiveData<String> = removedSingleCategoryLiveEvent
    fun getRemovedMultipleCategoriesLiveEvent(): LiveData<String> =
        removedMultipleCategoriesLiveEvent

    fun getErrorLiveEvent(): LiveData<Throwable> = errorLiveEvent

    init {
        retrieveData()
    }

    private fun retrieveData() {
        categoryRepository.getCategoryList().subscribeByIO(
            onSuccess = { categories ->
                categoryListLiveData.postValue(categories.map { CategoryDataWrapper(it) })
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun saveCategory(name: String) {
        categoryRepository.saveCategory(name).subscribeByIO(
            onComplete = {
                retrieveData()
                Timber.i("Saved category : $name")
                savedCategoryLiveEvent.postValue(name)
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun updateCategory(
        id: Int,
        oldName: String,
        newName: String
    ) {
        categoryRepository.updateCategory(id, newName).subscribeByIO(
            onComplete = {
                retrieveData()
                Timber.i("Category update : $oldName to $newName")
                updatedCategoryLiveEvent.postValue(oldName to newName)
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun removeCategories(categoryToDeleteList: List<CategoryDataWrapper>) {
        val categoryToDeleteListString = categoryToDeleteList.joinToString { it.getName() }
        val categoryToDeleteListId = categoryToDeleteList.map { it.getId() }

        categoryRepository.updateFoodsAndRemoveCategories(categoryToDeleteListId).subscribeByIO(
            onComplete = {
                retrieveData()
                Timber.i("Foods updated with other category and next categories deleted : $categoryToDeleteList")
                if (categoryToDeleteList.count() == 1) {
                    removedSingleCategoryLiveEvent.postValue(categoryToDeleteListString)
                } else {
                    removedMultipleCategoriesLiveEvent.postValue(
                        categoryToDeleteListString
                    )
                }
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }
}
