package com.marchenaya.presentation.ui.addFood

interface AddFoodFragmentNavigatorListener {

    fun displayFoodListFragment()
}
