package com.marchenaya.presentation.ui.history.item.parent

import android.content.Context
import com.marchenaya.presentation.R
import com.marchenaya.presentation.databinding.HistoryGroupListItemBinding
import com.marchenaya.presentation.extensions.convertToString
import java.util.Date

class HistoryGroupListFragmentViewHolder(private val binding: HistoryGroupListItemBinding) {

    fun bind(addDate: Date, context: Context) {
        binding.historyGroupDate.text =
            addDate.convertToString(context.getString(R.string.history_date_format))
    }
}
