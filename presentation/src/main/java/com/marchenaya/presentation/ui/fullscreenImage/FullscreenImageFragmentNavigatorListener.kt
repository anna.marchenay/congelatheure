package com.marchenaya.presentation.ui.fullscreenImage

interface FullscreenImageFragmentNavigatorListener {

    fun displayDetailFoodFragment(foodId: Int, foodName: String)
}
