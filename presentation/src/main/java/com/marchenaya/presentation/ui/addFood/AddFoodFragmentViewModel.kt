package com.marchenaya.presentation.ui.addFood

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.marchenaya.data.model.Category
import com.marchenaya.data.model.Food
import com.marchenaya.data.model.Location
import com.marchenaya.data.repository.CategoryRepository
import com.marchenaya.data.repository.FoodRepository
import com.marchenaya.data.repository.LocationRepository
import com.marchenaya.presentation.base.SingleLiveEvent
import com.marchenaya.presentation.base.viewmodel.BaseViewModel
import com.marchenaya.presentation.extensions.subscribeByIO
import com.marchenaya.presentation.wrapper.CategoryDataWrapper
import com.marchenaya.presentation.wrapper.LocationDataWrapper
import timber.log.Timber
import java.util.Calendar
import java.util.Date
import javax.inject.Inject

class AddFoodFragmentViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository,
    private val locationRepository: LocationRepository,
    private val foodRepository: FoodRepository
) : BaseViewModel() {

    private val categoryListLiveData = MutableLiveData<List<CategoryDataWrapper>>()
    private val locationListLiveData = MutableLiveData<List<LocationDataWrapper>>()
    private val savedFoodLiveEvent = SingleLiveEvent<String>()
    private val errorLiveEvent = SingleLiveEvent<Throwable>()

    fun getCategoryListLiveData(): LiveData<List<CategoryDataWrapper>> = categoryListLiveData
    fun getLocationListLiveData(): LiveData<List<LocationDataWrapper>> = locationListLiveData
    fun getSavedFoodLiveEvent(): LiveData<String> = savedFoodLiveEvent

    fun getErrorLiveEvent(): LiveData<Throwable> = errorLiveEvent

    init {
        retrieveCategoryList()
        retrieveLocationList()
    }

    private fun retrieveCategoryList() {
        categoryRepository.getCategoryList().subscribeByIO(
            onSuccess = { categories ->
                categoryListLiveData.postValue(categories.map { CategoryDataWrapper(it) })
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    private fun retrieveLocationList() {
        locationRepository.getLocationList().subscribeByIO(
            onSuccess = { locations ->
                locationListLiveData.postValue(locations.map { LocationDataWrapper(it) })
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun saveFood(
        name: String,
        limitDate: Date?,
        quantity: Int,
        photoUri: Uri,
        locationDataWrapper: LocationDataWrapper,
        categoryDataWrapper: CategoryDataWrapper
    ) {
        foodRepository.saveFood(
            Food(
                name = name,
                limitDate = limitDate,
                addDate = Calendar.getInstance().time,
                modifiedDate = Calendar.getInstance().time,
                imagePath = photoUri.toString(),
                quantity = quantity,
                location = Location(locationDataWrapper.getId(), locationDataWrapper.getName()),
                category = Category(categoryDataWrapper.getId(), categoryDataWrapper.getName())
            )
        ).subscribeByIO(
            onComplete = {
                Timber.i("Saved food : $name")
                savedFoodLiveEvent.postValue(name)
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }
}
