package com.marchenaya.presentation.ui.history

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.marchenaya.data.extensions.getDayDate
import com.marchenaya.data.model.HistoryDescription
import com.marchenaya.data.repository.HistoryRepository
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.SingleLiveEvent
import com.marchenaya.presentation.base.viewmodel.BaseViewModel
import com.marchenaya.presentation.extensions.subscribeByIO
import com.marchenaya.presentation.utils.HistoryDescriptionUtils.getHistoryDescription
import com.marchenaya.presentation.wrapper.HistoryDataWrapper
import java.util.Date
import java.util.SortedMap
import javax.inject.Inject
import timber.log.Timber

class HistoryFragmentViewModel @Inject constructor(private val historyRepository: HistoryRepository) :
    BaseViewModel() {

    private val historyMapLiveData = MutableLiveData<SortedMap<Date, List<HistoryDataWrapper>>>()
    private val errorLiveEvent = SingleLiveEvent<Throwable>()

    fun getHistoryListLiveData(): LiveData<SortedMap<Date, List<HistoryDataWrapper>>> =
        historyMapLiveData

    fun getErrorLiveEvent(): LiveData<Throwable> = errorLiveEvent

    init {
        retrieveData()
    }

    private fun retrieveData() {
        historyRepository.getHistoryList().subscribeByIO(
            onSuccess = { histories ->
                val sortedMap: SortedMap<Date, List<HistoryDataWrapper>> = sortedMapOf()
                histories.map { it.date.getDayDate() }.forEach { date ->
                    sortedMap[date] = histories.map { HistoryDataWrapper(it) }
                        .filter { it.getDate().getDayDate() == date }
                }
                historyMapLiveData.postValue(sortedMap)
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun search(query: String, context: Context) {
        val sortedMap: SortedMap<Date, List<HistoryDataWrapper>> = sortedMapOf()
        val historyDescription = HistoryDescription(
            context.getString(R.string.history_category_modified),
            context.getString(R.string.history_category_removed),
            context.getString(R.string.history_category_added),
            context.getString(R.string.history_location_modified),
            context.getString(R.string.history_location_removed),
            context.getString(R.string.history_location_added),
            context.getString(R.string.history_food_modified),
            context.getString(R.string.history_food_removed),
            context.getString(R.string.history_food_added),
            context.getString(R.string.history_food_modified_name),
            context.getString(R.string.history_food_modified_limit_date),
            context.getString(R.string.history_food_modified_null_limit_date),
            context.getString(R.string.history_food_modified_image),
            context.getString(R.string.history_food_modified_quantity),
            context.getString(R.string.history_food_modified_category),
            context.getString(R.string.history_food_modified_location),
            context.getString(R.string.limit_date_format)
        )
        historyRepository.searchHistory(
            query,
            context.getString(R.string.history_date_format),
            context.getString(R.string.history_hour_format),
            { historyDescriptionFunction, type, oldObject, newObject ->
                getHistoryDescription(historyDescriptionFunction, type, oldObject, newObject)
            },
            historyDescription
        ).subscribeByIO(
            onSuccess = { histories ->
                histories.map { it.date.getDayDate() }.forEach { date ->
                    sortedMap[date] = histories.map { HistoryDataWrapper(it) }
                        .filter { it.getDate().getDayDate() == date }
                }
                historyMapLiveData.postValue(sortedMap)
            }, onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }
}
