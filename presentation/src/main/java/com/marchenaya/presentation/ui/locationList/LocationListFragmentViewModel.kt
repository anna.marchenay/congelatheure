package com.marchenaya.presentation.ui.locationList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.marchenaya.data.repository.LocationRepository
import com.marchenaya.presentation.base.SingleLiveEvent
import com.marchenaya.presentation.base.viewmodel.BaseViewModel
import com.marchenaya.presentation.extensions.subscribeByIO
import com.marchenaya.presentation.wrapper.LocationDataWrapper
import javax.inject.Inject
import timber.log.Timber

class LocationListFragmentViewModel @Inject constructor(
    private val locationRepository: LocationRepository
) :
    BaseViewModel() {

    private val locationListLiveData = MutableLiveData<List<LocationDataWrapper>>()
    private val savedLocationLiveEvent = SingleLiveEvent<String>()
    private val updatedLocationLiveEvent = SingleLiveEvent<Pair<String, String>>()
    private val removedSingleLocationLiveEvent = SingleLiveEvent<String>()
    private val removedMultipleLocationsLiveEvent = SingleLiveEvent<String>()
    private val errorLiveEvent = SingleLiveEvent<Throwable>()

    fun getLocationListLiveData(): LiveData<List<LocationDataWrapper>> = locationListLiveData
    fun getSavedLocationLiveEvent(): LiveData<String> = savedLocationLiveEvent
    fun getUpdatedLocationLiveEvent(): LiveData<Pair<String, String>> = updatedLocationLiveEvent
    fun getRemovedSingleLocationLiveEvent(): LiveData<String> = removedSingleLocationLiveEvent
    fun getRemovedMultipleLocationsLiveEvent(): LiveData<String> = removedMultipleLocationsLiveEvent
    fun getErrorLiveEvent(): LiveData<Throwable> = errorLiveEvent

    init {
        retrieveData()
    }

    private fun retrieveData() {
        locationRepository.getLocationList().subscribeByIO(
            onSuccess = { locations ->
                locationListLiveData.postValue(locations.map { LocationDataWrapper(it) })
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun saveLocation(name: String) {
        locationRepository.saveLocation(name).subscribeByIO(
            onComplete = {
                retrieveData()
                Timber.i("Saved location : $name")
                savedLocationLiveEvent.postValue(name)
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun updateLocation(
        id: Int,
        oldName: String,
        newName: String
    ) {
        locationRepository.updateLocation(id, newName).subscribeByIO(
            onComplete = {
                retrieveData()
                Timber.i("Location update : $oldName to $newName")
                updatedLocationLiveEvent.postValue(oldName to newName)
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun removeLocations(locationToDeleteList: List<LocationDataWrapper>) {
        val locationToDeleteListString = locationToDeleteList.joinToString { it.getName() }
        val locationToDeleteListId = locationToDeleteList.map { it.getId() }

        locationRepository.updateFoodsAndRemoveLocations(locationToDeleteListId)
            .subscribeByIO(onComplete = {
                retrieveData()
                Timber.i("Foods updated with undefined location and next locations deleted : $locationToDeleteList")
                if (locationToDeleteList.count() == 1) {
                    removedSingleLocationLiveEvent.postValue(locationToDeleteListString)
                } else {
                    removedMultipleLocationsLiveEvent.postValue(
                        locationToDeleteListString
                    )
                }
            }, onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            })
    }
}
