package com.marchenaya.presentation.ui.foodList

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.SearchView
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.chip.Chip
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.fragment.BaseVMFragment
import com.marchenaya.presentation.component.materialcardview.MaterialCardViewComponent
import com.marchenaya.presentation.component.snackbar.SnackbarComponent
import com.marchenaya.presentation.databinding.FragmentFoodListBinding
import com.marchenaya.presentation.extensions.getColorFromAttr
import com.marchenaya.presentation.extensions.hide
import com.marchenaya.presentation.extensions.observeSafe
import com.marchenaya.presentation.extensions.show
import com.marchenaya.presentation.ui.foodList.item.FoodListFragmentAdapter
import com.marchenaya.presentation.ui.sortFood.SortFoodModalBottomSheetFragment
import com.marchenaya.presentation.utils.GlobalValueHelper
import javax.inject.Inject

class FoodListFragment : BaseVMFragment<FoodListFragmentViewModel, FragmentFoodListBinding>() {

    @Inject
    lateinit var navigatorListener: FoodListFragmentNavigatorListener

    @Inject
    lateinit var foodListFragmentAdapter: FoodListFragmentAdapter

    @Inject
    lateinit var snackbarComponent: SnackbarComponent

    @Inject
    lateinit var materialCardViewComponent: MaterialCardViewComponent

    private var searchValue: String = ""

    private var addItem: MenuItem? = null

    override val viewModelClass = FoodListFragmentViewModel::class

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentFoodListBinding =
        FragmentFoodListBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.activity_main_menu_add -> {
            navigatorListener.displayAddFoodFragment()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val menuItem = menu.findItem(R.id.activity_main_menu_search)
        val searchView = menuItem.actionView as SearchView

        addItem = menuItem
        displayInitMessage()

        searchView.queryHint = getString(R.string.search_menu_button)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?) = false
            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    searchValue = it
                    viewModel.retrieveFoodListWithCriteria(
                        GlobalValueHelper.globalFoodSorter,
                        GlobalValueHelper.globalFilteredCategoryIdList,
                        searchValue
                    )
                }
                return false
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.retrieveFoodListWithCriteria(
            GlobalValueHelper.globalFoodSorter,
            GlobalValueHelper.globalFilteredCategoryIdList,
            searchValue
        )
        setupSortChip()
        setupGroupChip()
        setupRecyclerView()
        setupSelection()
        observeFoodList()
        observeEvents()
    }

    private fun setupSortChip() {
        binding {
            val sortModalBottomSheetFragment = SortFoodModalBottomSheetFragment()
            sortModalBottomSheetFragment.foodSorter = GlobalValueHelper.globalFoodSorter
            sortModalBottomSheetFragment.getSortLiveData()
                .observeSafe(viewLifecycleOwner) { sortEnum ->
                    GlobalValueHelper.globalFoodSorter = sortEnum
                    viewModel.retrieveFoodListWithCriteria(
                        sortEnum,
                        GlobalValueHelper.globalFilteredCategoryIdList,
                        searchValue
                    )
                }
            sortModalBottomSheetFragment.getShowDownArrow()
                .observeSafe(viewLifecycleOwner) { showDownArrow ->
                    if (showDownArrow) {
                        fragmentFoodSortChip.setChipIconResource(R.drawable.ic_down_arrow)
                    }
                }
            fragmentFoodSortChip.setOnClickListener {
                if (!sortModalBottomSheetFragment.isOpen) {
                    sortModalBottomSheetFragment.show(
                        requireActivity().supportFragmentManager,
                        SortFoodModalBottomSheetFragment.TAG
                    )
                    fragmentFoodSortChip.chipIcon = getPrimaryColorDrawable(R.drawable.ic_up_arrow)
                }
            }
        }
    }

    private fun getPrimaryColorDrawable(@DrawableRes drawableId: Int): Drawable? {
        val drawable = AppCompatResources.getDrawable(requireContext(), drawableId)
        if (drawable != null) {
            DrawableCompat.setTint(drawable, requireContext().getColorFromAttr(R.attr.colorPrimary))
        }
        return drawable
    }

    private fun setupGroupChip() {
        binding {
            viewModel.getCategoryListLiveData().observeSafe(viewLifecycleOwner) {
                it.forEach { categoryDataWrapper ->
                    with(
                        layoutInflater.inflate(
                            R.layout.chip,
                            fragmentFoodChipGroup,
                            false
                        ) as Chip
                    ) {
                        text = categoryDataWrapper.getName()
                        tag = categoryDataWrapper.getId()
                        fragmentFoodChipGroup.addView(this)
                        if (GlobalValueHelper.globalFilteredCategoryIdList.contains(tag as Int)) {
                            fragmentFoodChipGroup.check(id)
                        }
                        setupChipListener(this)
                    }
                }
            }
        }
    }

    private fun setupChipListener(chip: Chip) {
        binding {
            chip.setOnCheckedChangeListener { _, _ ->
                GlobalValueHelper.globalFilteredCategoryIdList =
                    if (fragmentFoodChipGroup.checkedChipIds.isEmpty()) {
                        listOf()
                    } else {
                        fragmentFoodChipGroup.checkedChipIds.map { id ->
                            (fragmentFoodChipGroup.findViewById<Chip>(id).tag) as Int
                        }
                    }
                viewModel.retrieveFoodListWithCriteria(
                    GlobalValueHelper.globalFoodSorter,
                    GlobalValueHelper.globalFilteredCategoryIdList,
                    searchValue
                )
            }
        }
    }

    private fun setupRecyclerView() {
        foodListFragmentAdapter.onItemClickListener = { foodId, foodName ->
            navigatorListener.displayFoodDetailFragment(foodId, foodName)
        }

        binding.fragmentFoodListRecyclerView.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = foodListFragmentAdapter
        }
    }

    private fun setupSelection() {
        materialCardViewComponent.selectCards(
            activity = requireActivity() as AppCompatActivity,
            recyclerView = binding.fragmentFoodListRecyclerView,
            onRemoveButtonClicked = { selection ->
                viewModel.removeFoods(
                    foodListFragmentAdapter.getCurrentFoodIds(selection)
                )
            },
            setSelectionTracker = { selectionTracker ->
                foodListFragmentAdapter.setSelectionTracker(selectionTracker)
            }
        )
    }

    private fun observeFoodList() {
        binding {
            viewModel.getFoodListLiveData().observeSafe(viewLifecycleOwner) {
                foodListFragmentAdapter.setItems(it)
                displayInitMessage()
            }
        }
    }

    private fun displayInitMessage() {
        binding {
            if (foodListFragmentAdapter.itemCount == 0 &&
                searchValue.isEmpty() &&
                GlobalValueHelper.globalFilteredCategoryIdList.isEmpty()
            ) {
                fragmentFoodNestedConstraintLayout.hide()
                fragmentFoodListInitText.show()
                addItem?.hide()
            } else {
                fragmentFoodNestedConstraintLayout.show()
                fragmentFoodListInitText.hide()
                addItem?.show()
            }
        }
    }

    private fun observeEvents() {
        viewModel.getRemovedSingleFoodLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.removed_single_food, it),
                view
            )
            binding
        }
        viewModel.getRemovedMultipleFoodsLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.removed_multiple_foods, it),
                view
            )
        }
        viewModel.getErrorLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displayError(requireContext(), it, view)
        }
    }

    override fun onDestroy() {
        materialCardViewComponent.finishActionMode()
        super.onDestroy()
    }
}
