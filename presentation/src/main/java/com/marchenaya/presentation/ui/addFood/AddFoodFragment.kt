package com.marchenaya.presentation.ui.addFood

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.fragment.BaseVMFragment
import com.marchenaya.presentation.component.dialog.DialogComponent
import com.marchenaya.presentation.component.file.FileComponent
import com.marchenaya.presentation.component.intent.IntentComponent
import com.marchenaya.presentation.component.snackbar.SnackbarComponent
import com.marchenaya.presentation.databinding.FragmentAddFoodBinding
import com.marchenaya.presentation.extensions.observeSafe
import com.marchenaya.presentation.utils.FieldValidatorUtils
import javax.inject.Inject

class AddFoodFragment : BaseVMFragment<AddFoodFragmentViewModel, FragmentAddFoodBinding>() {

    @Inject
    lateinit var dialogComponent: DialogComponent

    @Inject
    lateinit var snackbarComponent: SnackbarComponent

    @Inject
    lateinit var fileComponent: FileComponent

    @Inject
    lateinit var intentComponent: IntentComponent

    @Inject
    lateinit var fieldValidatorUtils: FieldValidatorUtils

    @Inject
    lateinit var navigatorListener: AddFoodFragmentNavigatorListener

    override val viewModelClass = AddFoodFragmentViewModel::class

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentAddFoodBinding =
        FragmentAddFoodBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        intentComponent.setupIntents(requireContext(), this, binding)
        setupForm()
        setupSpinners()
        observeEvents()
    }

    private fun setupForm() {
        binding.addFoodForm.setupAddForm(
            fileComponent,
            dialogComponent,
            intentComponent,
            fieldValidatorUtils
        ) { name, limitDate, quantity, photoUri, locationDataWrapper, categoryDataWrapper ->
            viewModel.saveFood(
                name,
                limitDate,
                quantity,
                photoUri,
                locationDataWrapper,
                categoryDataWrapper
            )
        }
    }

    private fun setupSpinners() {
        binding {
            viewModel.getCategoryListLiveData().observeSafe(viewLifecycleOwner) {
                addFoodForm.setupCategorySpinner(it)
            }
            viewModel.getLocationListLiveData().observeSafe(viewLifecycleOwner) {
                addFoodForm.setupLocationSpinner(it)
            }
        }
    }

    private fun observeEvents() {
        viewModel.getSavedFoodLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.added_food, it),
                view
            )
            navigatorListener.displayFoodListFragment()
        }

        viewModel.getErrorLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displayError(requireContext(), it, view)
        }
    }
}
