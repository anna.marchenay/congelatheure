package com.marchenaya.presentation.ui.foodList.item

import androidx.recyclerview.widget.DiffUtil
import com.marchenaya.presentation.wrapper.FoodDataWrapper

class FoodItemDiffCallback : DiffUtil.ItemCallback<FoodDataWrapper>() {

    override fun areItemsTheSame(
        oldItem: FoodDataWrapper,
        newItem: FoodDataWrapper
    ): Boolean =
        oldItem.getId() == newItem.getId()

    override fun areContentsTheSame(
        oldItem: FoodDataWrapper,
        newItem: FoodDataWrapper
    ): Boolean =
        oldItem.getId() == newItem.getId() && oldItem.getName() == newItem.getName()
}
