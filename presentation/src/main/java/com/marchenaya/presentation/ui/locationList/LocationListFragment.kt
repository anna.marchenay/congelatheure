package com.marchenaya.presentation.ui.locationList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.fragment.BaseVMFragment
import com.marchenaya.presentation.component.dialog.DialogComponent
import com.marchenaya.presentation.component.materialcardview.MaterialCardViewComponent
import com.marchenaya.presentation.component.snackbar.SnackbarComponent
import com.marchenaya.presentation.databinding.FragmentLocationListBinding
import com.marchenaya.presentation.extensions.observeSafe
import com.marchenaya.presentation.ui.locationList.item.LocationListFragmentAdapter
import javax.inject.Inject

class LocationListFragment @Inject constructor() :
    BaseVMFragment<LocationListFragmentViewModel, FragmentLocationListBinding>() {

    @Inject
    lateinit var locationListFragmentAdapter: LocationListFragmentAdapter

    @Inject
    lateinit var dialogComponent: DialogComponent

    @Inject
    lateinit var materialCardViewComponent: MaterialCardViewComponent

    @Inject
    lateinit var snackbarComponent: SnackbarComponent

    override val viewModelClass = LocationListFragmentViewModel::class

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentLocationListBinding =
        FragmentLocationListBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.activity_main_menu_add -> {

            dialogComponent.displayEditTextDialog(
                requireContext(),
                R.string.location_list_add_dialog_title,
                R.string.location_list_dialog_hint,
                R.string.location_list_dialog_positive,
                true
            ) { name ->
                viewModel.saveLocation(name)
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupSelection()
        observeLocation()
        observeEvents()
    }

    private fun setupRecyclerView() {
        locationListFragmentAdapter.onItemClickListener = { id, oldName ->
            dialogComponent.displayEditTextDialog(
                requireContext(),
                R.string.location_list_update_dialog_title,
                R.string.location_list_dialog_hint,
                R.string.location_list_update_dialog_positive,
                true,
                oldName
            ) { newName ->
                viewModel.updateLocation(id, oldName, newName)
            }
        }
        binding.fragmentLocationListRecyclerView.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = locationListFragmentAdapter
        }
    }

    private fun setupSelection() {
        materialCardViewComponent.selectCards(
            activity = requireActivity() as AppCompatActivity,
            recyclerView = binding.fragmentLocationListRecyclerView,
            onRemoveButtonClicked = { selection ->
                viewModel.removeLocations(
                    locationListFragmentAdapter.getCurrentLocationIds(selection)
                )
            },
            getPosition = { locationListFragmentAdapter.getUndefinedLocationPosition() },
            setSelectionTracker = { selectionTracker ->
                locationListFragmentAdapter.setSelectionTracker(selectionTracker)
            },
            displayDialog = { onPositiveClick ->
                dialogComponent.displayRemoveWarningDialog(
                    context = requireContext(),
                    title = R.string.location_list_remove_warning_title,
                    message = R.string.location_list_remove_warning_message,
                    positiveText = android.R.string.ok,
                    negativeText = R.string.location_list_remove_warning_negative_text,
                    onPositiveClick = onPositiveClick
                )
            }
        )
    }

    private fun observeLocation() {
        viewModel.getLocationListLiveData().observeSafe(viewLifecycleOwner) {
            locationListFragmentAdapter.setItems(it)
        }
    }

    private fun observeEvents() {
        viewModel.getSavedLocationLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.added_location, it),
                view
            )
        }
        viewModel.getUpdatedLocationLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.updated_location, it.first, it.second),
                view
            )
        }
        viewModel.getRemovedSingleLocationLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.removed_single_location, it),
                view
            )
        }
        viewModel.getRemovedMultipleLocationsLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.removed_multiple_locations, it),
                view
            )
        }
        viewModel.getErrorLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displayError(requireContext(), it, view)
        }
    }

    override fun onDestroy() {
        materialCardViewComponent.finishActionMode()
        super.onDestroy()
    }
}
