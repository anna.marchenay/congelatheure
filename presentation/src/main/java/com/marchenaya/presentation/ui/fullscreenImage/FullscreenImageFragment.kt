package com.marchenaya.presentation.ui.fullscreenImage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.fragment.BaseFragment
import com.marchenaya.presentation.databinding.FragmentFullscreenImageBinding
import javax.inject.Inject

class FullscreenImageFragment : BaseFragment<FragmentFullscreenImageBinding>() {

    @Inject
    lateinit var navigatorListener: FullscreenImageFragmentNavigatorListener

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentFullscreenImageBinding =
        FragmentFullscreenImageBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: FullscreenImageFragmentArgs by navArgs()
        binding {
            Glide.with(view)
                .load(args.imagePath)
                .placeholder(R.drawable.ic_image)
                .into(fullscreenImage)
            fullscreenImage.setOnClickListener {
                navigatorListener.displayDetailFoodFragment(
                    args.foodId,
                    args.foodName
                )
            }
        }
    }
}
