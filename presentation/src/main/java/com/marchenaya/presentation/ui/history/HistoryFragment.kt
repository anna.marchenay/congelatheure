package com.marchenaya.presentation.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import com.marchenaya.presentation.base.fragment.BaseVMFragment
import com.marchenaya.presentation.component.snackbar.SnackbarComponent
import com.marchenaya.presentation.databinding.FragmentHistoryBinding
import com.marchenaya.presentation.extensions.observeSafe
import com.marchenaya.presentation.ui.history.item.HistoryListFragmentAdapter
import java.util.Date
import javax.inject.Inject

class HistoryFragment : BaseVMFragment<HistoryFragmentViewModel, FragmentHistoryBinding>() {

    @Inject
    lateinit var adapter: HistoryListFragmentAdapter

    @Inject
    lateinit var snackbarComponent: SnackbarComponent

    private val expandedHistoryDateList = mutableListOf<Date>()
    private var previousHistoryDateList = mutableListOf<Date>()
    private var queryText = ""

    override val viewModelClass = HistoryFragmentViewModel::class

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentHistoryBinding =
        FragmentHistoryBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupExpandableListView()
        observeHistory()
        observeError()
    }

    private fun setupExpandableListView() {
        binding {
            fragmentHistoryExpandableListView.dividerHeight = 0
            fragmentHistoryExpandableListView.setAdapter(adapter)
            fragmentHistoryExpandableListView.setOnGroupExpandListener { index ->
                if (previousHistoryDateList.isNotEmpty()) {
                    expandedHistoryDateList.add(previousHistoryDateList[index])
                }
            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val menuItem = menu.findItem(com.marchenaya.presentation.R.id.activity_main_menu_search)
        val searchView = menuItem.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = false
            override fun onQueryTextChange(newText: String?): Boolean {
                queryText = newText ?: ""
                newText?.let { viewModel.search(it, requireContext()) }
                return false
            }
        })
    }

    private fun observeHistory() {
        viewModel.getHistoryListLiveData().observeSafe(viewLifecycleOwner) { sortedMap ->
            previousHistoryDateList = ArrayList(sortedMap.keys)
            adapter.setItems(ArrayList(sortedMap.keys), sortedMap)
            if (queryText.isNotEmpty()) {
                expandedHistoryDateList.clear()
                previousHistoryDateList.forEach { date ->
                    val index = sortedMap.keys.indexOf(date)
                    binding.fragmentHistoryExpandableListView.expandGroup(index)
                    expandedHistoryDateList.add(index, date)
                }
            } else {
                previousHistoryDateList.forEach { date ->
                    val index =
                        sortedMap.keys.indexOf(date) + (previousHistoryDateList.count() - sortedMap.keys.count())
                    if (expandedHistoryDateList.contains(date)) {
                        binding.fragmentHistoryExpandableListView.expandGroup(index)
                    } else {
                        binding.fragmentHistoryExpandableListView.collapseGroup(index)
                    }
                }
                expandedHistoryDateList.clear()
            }
        }
    }

    private fun observeError() {
        viewModel.getErrorLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displayError(requireContext(), it, view)
        }
    }
}
