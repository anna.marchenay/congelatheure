package com.marchenaya.presentation.ui.categoryList.item

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.Selection
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.ListAdapter
import com.marchenaya.presentation.databinding.CategoryListItemBinding
import com.marchenaya.presentation.wrapper.CategoryDataWrapper
import javax.inject.Inject

class CategoryListFragmentAdapter @Inject constructor() :
    ListAdapter<CategoryDataWrapper, CategoryListFragmentViewHolder>(CategoryItemDiffCallback()) {

    private val items: MutableList<CategoryDataWrapper> = mutableListOf()

    private lateinit var selectionTracker: SelectionTracker<Long>

    lateinit var onItemClickListener: (Int, String) -> Unit

    fun getCurrentCategoryIds(position: Selection<Long>) = position.map {
        items[it.toInt()]
    }

    fun getOtherCategoryPosition() = items.indexOfFirst { it.getId() == 1 }.toLong()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryListFragmentViewHolder =
        CategoryListFragmentViewHolder(
            CategoryListItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: CategoryListFragmentViewHolder, position: Int) {
        holder.bind(items[position], position, selectionTracker, onItemClickListener)
    }

    override fun getItemCount(): Int = items.size

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(newItems: List<CategoryDataWrapper>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun setSelectionTracker(selectionTracker: SelectionTracker<Long>) {
        this.selectionTracker = selectionTracker
    }
}
