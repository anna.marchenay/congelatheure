package com.marchenaya.presentation.ui.locationList.item

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemDetailsLookup.ItemDetails
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.marchenaya.presentation.component.dialog.DialogComponent
import com.marchenaya.presentation.databinding.LocationListItemBinding
import com.marchenaya.presentation.wrapper.LocationDataWrapper
import javax.inject.Inject

class LocationListFragmentViewHolder(private val binding: LocationListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    @Inject
    lateinit var dialogComponent: DialogComponent

    private val details = object : ItemDetailsLookup.ItemDetails<Long>() {
        var position: Long = 0
        override fun getPosition(): Int {
            return position.toInt()
        }

        override fun getSelectionKey(): Long {
            return position
        }

        override fun inSelectionHotspot(e: MotionEvent): Boolean {
            return false
        }

        override fun inDragRegion(e: MotionEvent): Boolean {
            return true
        }
    }

    fun bind(
        locationDataWrapper: LocationDataWrapper,
        position: Int,
        selectionTracker: SelectionTracker<Long>,
        onItemClickListener: (Int, String) -> Unit
    ) {
        with(binding) {
            locationCard.isEnabled = locationDataWrapper.getId() != 1
            details.position = position.toLong()
            locationListItemName.text = locationDataWrapper.getName()
            locationCard.isChecked = selectionTracker.isSelected(details.selectionKey)
            root.setOnClickListener {
                onItemClickListener(locationDataWrapper.getId(), locationDataWrapper.getName())
            }
        }
    }

    fun getItemDetails(): ItemDetails<Long> {
        return details
    }
}
