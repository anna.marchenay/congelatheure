package com.marchenaya.presentation.ui.history.item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import com.bumptech.glide.Glide
import com.marchenaya.presentation.R
import com.marchenaya.presentation.databinding.HistoryChildListItemBinding
import com.marchenaya.presentation.databinding.HistoryGroupListItemBinding
import com.marchenaya.presentation.extensions.hide
import com.marchenaya.presentation.extensions.show
import com.marchenaya.presentation.ui.history.item.child.HistoryChildListFragmentViewHolder
import com.marchenaya.presentation.ui.history.item.parent.HistoryGroupListFragmentViewHolder
import com.marchenaya.presentation.wrapper.HistoryDataWrapper
import java.util.Date
import java.util.SortedMap
import javax.inject.Inject

class HistoryListFragmentAdapter @Inject constructor() : BaseExpandableListAdapter() {

    private var groupList: MutableList<Date> = mutableListOf()
    private var childList: SortedMap<Date, List<HistoryDataWrapper>> = sortedMapOf()

    override fun getGroupCount(): Int = groupList.size

    override fun getChildrenCount(listPosition: Int): Int =
        childList[groupList[listPosition]]?.size ?: -1

    override fun getGroup(listPosition: Int): Any = groupList[listPosition]

    override fun getChild(listPosition: Int, expandedListPosition: Int): Any =
        childList[groupList[listPosition]]?.get(expandedListPosition) ?: -1

    override fun getGroupId(listPosition: Int): Long = listPosition.toLong()

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long =
        expandedListPosition.toLong()

    override fun hasStableIds(): Boolean = false

    override fun getGroupView(
        listPosition: Int,
        isExpanded: Boolean,
        view: View?,
        parent: ViewGroup?
    ): View {
        var convertView = view
        val holder: HistoryGroupListFragmentViewHolder
        val binding: HistoryGroupListItemBinding
        val holderId = R.id.expandable_list_adapter_holder
        val bindingId = R.id.expandable_list_adapter_binding

        if (convertView == null) {
            binding = HistoryGroupListItemBinding.inflate(LayoutInflater.from(parent?.context))
            convertView = binding.root
            holder = HistoryGroupListFragmentViewHolder(binding)
            convertView.setTag(holderId, holder)
            convertView.setTag(bindingId, binding)
        } else {
            holder = convertView.getTag(holderId) as HistoryGroupListFragmentViewHolder
            binding = convertView.getTag(bindingId) as HistoryGroupListItemBinding
        }
        parent?.context?.let { holder.bind(getGroup(listPosition) as Date, it) }

        with(binding) {
            if (getChildrenCount(listPosition) == 0) {
                historyGroupHelper.hide()
            } else {
                Glide.with(convertView)
                    .load(if (isExpanded) R.drawable.ic_down_arrow else R.drawable.ic_up_arrow)
                    .circleCrop()
                    .into(historyGroupIndicator)
                historyGroupHelper.show()
            }
        }

        return convertView
    }

    override fun getChildView(
        listPosition: Int,
        expandedListPosition: Int,
        isLastChild: Boolean,
        view: View?,
        parent: ViewGroup
    ): View {
        var convertView = view
        val holder: HistoryChildListFragmentViewHolder
        if (convertView == null) {
            val childBinding =
                HistoryChildListItemBinding.inflate(LayoutInflater.from(parent.context))
            convertView = childBinding.root
            holder = HistoryChildListFragmentViewHolder(childBinding, parent.context)
            convertView.tag = holder
        } else {
            holder = convertView.tag as HistoryChildListFragmentViewHolder
        }
        val child = getChild(listPosition, expandedListPosition)
        if (child != -1) {
            holder.bind(child as HistoryDataWrapper)
        }
        return convertView
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean = false

    fun setItems(
        newGroupList: List<Date>,
        newChildList: SortedMap<Date, List<HistoryDataWrapper>>
    ) {
        groupList.clear()
        groupList.addAll(newGroupList)
        childList.clear()
        childList.putAll(newChildList)
        notifyDataSetChanged()
    }
}
