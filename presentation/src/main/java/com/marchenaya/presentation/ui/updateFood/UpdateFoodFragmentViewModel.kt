package com.marchenaya.presentation.ui.updateFood

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.marchenaya.data.model.Category
import com.marchenaya.data.model.Food
import com.marchenaya.data.model.Location
import com.marchenaya.data.repository.CategoryRepository
import com.marchenaya.data.repository.FoodRepository
import com.marchenaya.data.repository.LocationRepository
import com.marchenaya.presentation.base.SingleLiveEvent
import com.marchenaya.presentation.base.viewmodel.BaseViewModel
import com.marchenaya.presentation.extensions.subscribeByIO
import com.marchenaya.presentation.wrapper.CategoryDataWrapper
import com.marchenaya.presentation.wrapper.FoodDataWrapper
import com.marchenaya.presentation.wrapper.LocationDataWrapper
import timber.log.Timber
import java.util.Calendar
import java.util.Date
import javax.inject.Inject

class UpdateFoodFragmentViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository,
    private val locationRepository: LocationRepository,
    private val foodRepository: FoodRepository
) : BaseViewModel() {

    private val foodLiveData = MutableLiveData<FoodDataWrapper>()
    private val categoryListLiveData = MutableLiveData<List<CategoryDataWrapper>>()
    private val locationListLiveData = MutableLiveData<List<LocationDataWrapper>>()
    private val modifiedFoodLiveEvent = SingleLiveEvent<String>()
    private val errorLiveEvent = SingleLiveEvent<Throwable>()

    fun getFoodLiveData(): LiveData<FoodDataWrapper> = foodLiveData
    fun getCategoryListLiveData(): LiveData<List<CategoryDataWrapper>> = categoryListLiveData
    fun getLocationListLiveData(): LiveData<List<LocationDataWrapper>> = locationListLiveData
    fun getModifiedFoodLiveEvent(): LiveData<String> = modifiedFoodLiveEvent

    fun getErrorLiveEvent(): LiveData<Throwable> = errorLiveEvent

    init {
        retrieveCategoryList()
        retrieveLocationList()
    }

    private fun retrieveCategoryList() {
        categoryRepository.getCategoryList().subscribeByIO(
            onSuccess = { categories ->
                categoryListLiveData.postValue(categories.map { CategoryDataWrapper(it) })
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    private fun retrieveLocationList() {
        locationRepository.getLocationList().subscribeByIO(
            onSuccess = { locations ->
                locationListLiveData.postValue(locations.map { LocationDataWrapper(it) })
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun retrieveFood(id: Int) {
        foodRepository.getFood(id).subscribeByIO(
            onSuccess = {
                foodLiveData.postValue(FoodDataWrapper(it))
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun modifyFood(
        id: Int,
        name: String,
        limitDate: Date?,
        addDate: Date,
        quantity: Int,
        photoUri: Uri,
        locationDataWrapper: LocationDataWrapper,
        categoryDataWrapper: CategoryDataWrapper
    ) {
        val newFood = Food(
            id,
            name,
            limitDate,
            addDate,
            Calendar.getInstance().time,
            photoUri.toString(),
            quantity,
            Location(locationDataWrapper.getId(), locationDataWrapper.getName()),
            Category(categoryDataWrapper.getId(), categoryDataWrapper.getName())
        )

        foodRepository.updateFood(newFood).subscribeByIO(
            onComplete = {
                Timber.i("Modified food : $name")
                modifiedFoodLiveEvent.postValue(name)
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }
}
