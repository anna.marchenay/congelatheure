package com.marchenaya.presentation.ui.updateFood

interface UpdateFoodFragmentNavigatorListener {

    fun displayFoodDetailFragment(foodId: Int, foodName: String)
}
