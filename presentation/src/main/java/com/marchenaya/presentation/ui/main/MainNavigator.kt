package com.marchenaya.presentation.ui.main

import androidx.navigation.NavController
import com.marchenaya.presentation.component.navigation.NavigationComponent
import com.marchenaya.presentation.di.annotation.PerActivity
import com.marchenaya.presentation.ui.addFood.AddFoodFragmentDirections
import com.marchenaya.presentation.ui.addFood.AddFoodFragmentNavigatorListener
import com.marchenaya.presentation.ui.foodDetail.FoodDetailFragmentDirections
import com.marchenaya.presentation.ui.foodDetail.FoodDetailFragmentNavigatorListener
import com.marchenaya.presentation.ui.foodList.FoodListFragmentDirections
import com.marchenaya.presentation.ui.foodList.FoodListFragmentNavigatorListener
import com.marchenaya.presentation.ui.fullscreenImage.FullscreenImageFragmentDirections
import com.marchenaya.presentation.ui.fullscreenImage.FullscreenImageFragmentNavigatorListener
import com.marchenaya.presentation.ui.updateFood.UpdateFoodFragmentNavigatorListener
import dagger.Lazy
import javax.inject.Inject

@PerActivity
class MainNavigator @Inject constructor(
    private val navController: Lazy<NavController>,
    private val navigationComponent: NavigationComponent
) : AddFoodFragmentNavigatorListener,
    FoodListFragmentNavigatorListener,
    FoodDetailFragmentNavigatorListener,
    FullscreenImageFragmentNavigatorListener,
    UpdateFoodFragmentNavigatorListener {

    override fun displayFoodListFragment() {
        if (navigationComponent.isNavigationEventBlocked(navController.get())) {
            return
        }
        navController.get()
            .navigate(AddFoodFragmentDirections.actionGlobalFoodListFragment())
    }

    override fun displayAddFoodFragment() {
        if (navigationComponent.isNavigationEventBlocked(navController.get())) {
            return
        }
        navController.get()
            .navigate(FoodListFragmentDirections.actionFoodListFragmentToAddFoodFragment())
    }

    override fun displayFoodDetailFragment(foodId: Int, foodName: String) {
        if (navigationComponent.isNavigationEventBlocked(navController.get())) {
            return
        }
        navController.get()
            .navigate(
                FoodListFragmentDirections.actionGlobalFoodDetailFragment(
                    foodId,
                    foodName
                )
            )
    }

    override fun displayDetailFoodFragment(foodId: Int, foodName: String) {
        if (navigationComponent.isNavigationEventBlocked(navController.get())) {
            return
        }
        navController.get()
            .navigate(
                FullscreenImageFragmentDirections.actionFullscreenImageFragmentToFoodDetailFragment(
                    foodId,
                    foodName
                )
            )
    }

    override fun displayFullscreenImageFragment(imagePath: String, foodId: Int, foodName: String) {
        if (navigationComponent.isNavigationEventBlocked(navController.get())) {
            return
        }
        navController.get()
            .navigate(
                FoodDetailFragmentDirections.actionFoodDetailFragmentToFullscreenImageFragment(
                    imagePath,
                    foodId,
                    foodName
                )
            )
    }

    override fun displayUpdateFoodFragment(foodId: Int, foodName: String) {
        if (navigationComponent.isNavigationEventBlocked(navController.get())) {
            return
        }
        navController.get()
            .navigate(
                FoodDetailFragmentDirections.actionFoodDetailFragmentToUpdateFoodFragment(
                    foodId,
                    foodName
                )
            )
    }
}
