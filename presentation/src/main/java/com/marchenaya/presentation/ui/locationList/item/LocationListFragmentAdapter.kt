package com.marchenaya.presentation.ui.locationList.item

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.Selection
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.ListAdapter
import com.marchenaya.presentation.databinding.LocationListItemBinding
import com.marchenaya.presentation.wrapper.LocationDataWrapper
import javax.inject.Inject

class LocationListFragmentAdapter @Inject constructor() :
    ListAdapter<LocationDataWrapper, LocationListFragmentViewHolder>(LocationItemDiffCallback()) {

    private val items: MutableList<LocationDataWrapper> = mutableListOf()

    private lateinit var selectionTracker: SelectionTracker<Long>

    lateinit var onItemClickListener: (Int, String) -> Unit

    fun getCurrentLocationIds(position: Selection<Long>) = position.map {
        items[it.toInt()]
    }

    fun getUndefinedLocationPosition() = items.indexOfFirst { it.getId() == 1 }.toLong()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LocationListFragmentViewHolder =
        LocationListFragmentViewHolder(
            LocationListItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: LocationListFragmentViewHolder, position: Int) {
        holder.bind(items[position], position, selectionTracker, onItemClickListener)
    }

    override fun getItemCount(): Int = items.size

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(newItems: List<LocationDataWrapper>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun setSelectionTracker(selectionTracker: SelectionTracker<Long>) {
        this.selectionTracker = selectionTracker
    }
}
