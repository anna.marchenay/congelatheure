package com.marchenaya.presentation.ui.updateFood

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.fragment.BaseVMFragment
import com.marchenaya.presentation.component.dialog.DialogComponent
import com.marchenaya.presentation.component.file.FileComponent
import com.marchenaya.presentation.component.intent.IntentComponent
import com.marchenaya.presentation.component.snackbar.SnackbarComponent
import com.marchenaya.presentation.databinding.FragmentUpdateFoodBinding
import com.marchenaya.presentation.extensions.observeSafe
import com.marchenaya.presentation.utils.FieldValidatorUtils
import javax.inject.Inject

class UpdateFoodFragment :
    BaseVMFragment<UpdateFoodFragmentViewModel, FragmentUpdateFoodBinding>() {

    private lateinit var updateFoodFragmentArgs: UpdateFoodFragmentArgs

    @Inject
    lateinit var dialogComponent: DialogComponent

    @Inject
    lateinit var snackbarComponent: SnackbarComponent

    @Inject
    lateinit var fileComponent: FileComponent

    @Inject
    lateinit var fieldValidatorUtils: FieldValidatorUtils

    @Inject
    lateinit var navigatorListener: UpdateFoodFragmentNavigatorListener

    @Inject
    lateinit var intentComponent: IntentComponent

    override val viewModelClass = UpdateFoodFragmentViewModel::class

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentUpdateFoodBinding =
        FragmentUpdateFoodBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        getUpdateFoodFragmentArgs()
    }

    private fun getUpdateFoodFragmentArgs() {
        val args: UpdateFoodFragmentArgs by navArgs()
        updateFoodFragmentArgs = args
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        intentComponent.setupIntents(requireContext(), this, binding)
        setupSpinners()
        viewModel.retrieveFood(updateFoodFragmentArgs.foodId)
        fillForm()
        observeEvents()
    }

    private fun fillForm() {
        viewModel.getFoodLiveData().observeSafe(viewLifecycleOwner) { foodDataWrapper ->
            binding.updateFoodForm.setupUpdateForm(
                fileComponent,
                dialogComponent,
                intentComponent,
                fieldValidatorUtils,
                foodDataWrapper
            ) { name, limitDate, quantity, photoUri, locationDataWrapper, categoryDataWrapper ->
                viewModel.modifyFood(
                    foodDataWrapper.getId(),
                    name,
                    limitDate,
                    foodDataWrapper.getAddDate(),
                    quantity,
                    photoUri,
                    locationDataWrapper,
                    categoryDataWrapper
                )
            }
        }
    }

    private fun setupSpinners() {
        binding {
            viewModel.getCategoryListLiveData().observeSafe(viewLifecycleOwner) {
                updateFoodForm.setupCategorySpinner(it)
            }
            viewModel.getLocationListLiveData().observeSafe(viewLifecycleOwner) {
                updateFoodForm.setupLocationSpinner(it)
            }
        }
    }

    private fun observeEvents() {
        viewModel.getModifiedFoodLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.modified_food, it),
                view
            )
            navigatorListener.displayFoodDetailFragment(
                updateFoodFragmentArgs.foodId,
                it
            )
        }

        viewModel.getErrorLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displayError(requireContext(), it, view)
        }
    }
}
