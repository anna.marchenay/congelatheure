package com.marchenaya.presentation.ui.foodDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.fragment.BaseVMFragment
import com.marchenaya.presentation.component.snackbar.SnackbarComponent
import com.marchenaya.presentation.databinding.FragmentFoodDetailBinding
import com.marchenaya.presentation.extensions.convertToString
import com.marchenaya.presentation.extensions.hide
import com.marchenaya.presentation.extensions.observeSafe
import com.marchenaya.presentation.wrapper.FoodDataWrapper
import javax.inject.Inject

class FoodDetailFragment :
    BaseVMFragment<FoodDetailFragmentViewModel, FragmentFoodDetailBinding>() {

    private lateinit var foodDetailFragmentArgs: FoodDetailFragmentArgs

    @Inject
    lateinit var snackbarComponent: SnackbarComponent

    @Inject
    lateinit var navigatorListener: FoodDetailFragmentNavigatorListener

    override val viewModelClass = FoodDetailFragmentViewModel::class

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentFoodDetailBinding =
        FragmentFoodDetailBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        getFoodDetailFragmentArgs()
    }

    private fun getFoodDetailFragmentArgs() {
        val args: FoodDetailFragmentArgs by navArgs()
        foodDetailFragmentArgs = args
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fillFragment()
        observeEvents()
    }

    private fun fillFragment() {

        viewModel.retrieveFoodDetail(foodDetailFragmentArgs.foodId)

        viewModel.getFoodLiveData().observeSafe(viewLifecycleOwner) { foodDataWrapper ->

            val limitDate = foodDataWrapper.getLimitDate()
            val limitDateFormat = requireContext().getString(R.string.limit_date_format)
            val defaultDateFormat = requireContext().getString(R.string.default_date_format)

            binding {
                Glide.with(this@FoodDetailFragment)
                    .load(foodDataWrapper.getImagePath()).circleCrop()
                    .placeholder(R.drawable.ic_image)
                    .into(foodDetailImage)
                foodDetailCategory.text = foodDataWrapper.getCategoryName()
                foodDetailLocation.text = foodDataWrapper.getLocationName()
                foodDetailAddDate.text =
                    foodDataWrapper.getAddDate().convertToString(defaultDateFormat)
                if (limitDate != null) {
                    foodDetailLimitDate.text = limitDate.convertToString(limitDateFormat)
                } else {
                    foodDetailLimitDateTitle.hide()
                    foodDetailLimitDate.hide()
                }
                foodDetailQuantity.text = foodDataWrapper.getStringQuantity()
                foodDetailModifiedDate.text =
                    foodDataWrapper.getModifiedDate().convertToString(defaultDateFormat)

                setupClickListeners(foodDataWrapper)
            }
        }
    }

    private fun setupClickListeners(foodDataWrapper: FoodDataWrapper) {
        binding {

            foodDetailImage.setOnClickListener {
                navigatorListener.displayFullscreenImageFragment(
                    foodDataWrapper.getImagePath(),
                    foodDataWrapper.getId(),
                    foodDataWrapper.getName()
                )
            }

            foodDetailModifyButton.setOnClickListener {
                navigatorListener.displayUpdateFoodFragment(
                    foodDataWrapper.getId(),
                    foodDataWrapper.getName()
                )
            }

            foodDetailDeleteButton.setOnClickListener {
                viewModel.removeFood(foodDataWrapper)
            }
        }
    }

    private fun observeEvents() {

        viewModel.getRemovedFoodLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displaySuccess(
                requireContext(),
                getString(R.string.removed_single_food, it),
                view
            )
            navigatorListener.displayFoodListFragment()
        }

        viewModel.getErrorLiveEvent().observeSafe(viewLifecycleOwner) {
            snackbarComponent.displayError(requireContext(), it, view)
        }
    }
}
