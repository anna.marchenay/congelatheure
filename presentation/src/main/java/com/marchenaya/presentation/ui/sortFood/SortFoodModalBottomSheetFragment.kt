package com.marchenaya.presentation.ui.sortFood

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.marchenaya.data.model.FoodSorter
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.SingleLiveEvent
import com.marchenaya.presentation.base.viewmodel.ViewModelFactory
import com.marchenaya.presentation.databinding.SortFoodModalBottomSheetBinding
import javax.inject.Inject

class SortFoodModalBottomSheetFragment : BottomSheetDialogFragment() {

    var foodSorter: FoodSorter? = null
    var isOpen = false

    companion object {
        const val TAG = "SortFoodModalBottomSheetFragment"
    }

    private lateinit var binding: SortFoodModalBottomSheetBinding

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val sortLiveEvent = SingleLiveEvent<FoodSorter>()
    private val showDownArrowEvent = SingleLiveEvent<Boolean>()

    fun getSortLiveData(): LiveData<FoodSorter> = sortLiveEvent
    fun getShowDownArrow(): LiveData<Boolean> = showDownArrowEvent

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SortFoodModalBottomSheetBinding.inflate(inflater, container, false)
        setupGroups()
        return binding.root
    }

    private fun setupGroups() {
        with(binding) {
            when (foodSorter) {
                FoodSorter.NAME_ASC -> {
                    sortFoodModalBottomSheetRadioGroup.check(R.id.sort_food_modal_bottom_sheet_name_button)
                    sortFoodModalBottomSheetToggleGroup.check(R.id.sort_food_modal_bottom_sheet_ascending_button)
                }
                FoodSorter.NAME_DESC -> {
                    sortFoodModalBottomSheetRadioGroup.check(R.id.sort_food_modal_bottom_sheet_name_button)
                    sortFoodModalBottomSheetToggleGroup.check(R.id.sort_food_modal_bottom_sheet_descending_button)
                }
                FoodSorter.LIMIT_DATE_ASC -> {
                    sortFoodModalBottomSheetRadioGroup.check(R.id.sort_food_modal_bottom_sheet_expiration_date_button)
                    sortFoodModalBottomSheetToggleGroup.check(R.id.sort_food_modal_bottom_sheet_ascending_button)
                }
                FoodSorter.LIMIT_DATE_DESC -> {
                    sortFoodModalBottomSheetRadioGroup.check(R.id.sort_food_modal_bottom_sheet_expiration_date_button)
                    sortFoodModalBottomSheetToggleGroup.check(R.id.sort_food_modal_bottom_sheet_descending_button)
                }
                FoodSorter.ADD_DATE_ASC -> {
                    sortFoodModalBottomSheetRadioGroup.check(R.id.sort_food_modal_bottom_sheet_add_date_button)
                    sortFoodModalBottomSheetToggleGroup.check(R.id.sort_food_modal_bottom_sheet_ascending_button)
                }
                FoodSorter.ADD_DATE_DESC -> {
                    sortFoodModalBottomSheetRadioGroup.check(R.id.sort_food_modal_bottom_sheet_add_date_button)
                    sortFoodModalBottomSheetToggleGroup.check(R.id.sort_food_modal_bottom_sheet_descending_button)
                }
                null -> {
                    sortFoodModalBottomSheetRadioGroup.check(R.id.sort_food_modal_bottom_sheet_name_button)
                    sortFoodModalBottomSheetToggleGroup.check(R.id.sort_food_modal_bottom_sheet_ascending_button)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRadioGroupListener()
        setupToggleGroupListener()
    }

    override fun show(manager: FragmentManager, tag: String?) {
        super.show(manager, tag)
        isOpen = true
    }

    private fun setupRadioGroupListener() {
        with(binding) {
            sortFoodModalBottomSheetRadioGroup.setOnCheckedChangeListener { _, id ->
                when (id) {
                    R.id.sort_food_modal_bottom_sheet_name_button -> {
                        when (sortFoodModalBottomSheetToggleGroup.checkedButtonId) {
                            R.id.sort_food_modal_bottom_sheet_ascending_button -> {
                                foodSorter = FoodSorter.NAME_ASC
                            }
                            R.id.sort_food_modal_bottom_sheet_descending_button -> {
                                foodSorter = FoodSorter.NAME_DESC
                            }
                        }
                    }
                    R.id.sort_food_modal_bottom_sheet_expiration_date_button -> {
                        when (sortFoodModalBottomSheetToggleGroup.checkedButtonId) {
                            R.id.sort_food_modal_bottom_sheet_ascending_button -> {
                                foodSorter = FoodSorter.LIMIT_DATE_ASC
                            }
                            R.id.sort_food_modal_bottom_sheet_descending_button -> {
                                foodSorter = FoodSorter.LIMIT_DATE_DESC
                            }
                        }
                    }
                    R.id.sort_food_modal_bottom_sheet_add_date_button -> {
                        when (sortFoodModalBottomSheetToggleGroup.checkedButtonId) {
                            R.id.sort_food_modal_bottom_sheet_ascending_button -> {
                                foodSorter = FoodSorter.ADD_DATE_ASC
                            }
                            R.id.sort_food_modal_bottom_sheet_descending_button -> {
                                foodSorter = FoodSorter.ADD_DATE_DESC
                            }
                        }
                    }
                }
                sortLiveEvent.postValue(foodSorter)
            }
        }
    }

    private fun setupToggleGroupListener() {
        with(binding) {
            sortFoodModalBottomSheetToggleGroup.addOnButtonCheckedListener { _, checkedId, isChecked ->
                when (checkedId) {
                    R.id.sort_food_modal_bottom_sheet_ascending_button -> {
                        if (isChecked) {
                            when (sortFoodModalBottomSheetRadioGroup.checkedRadioButtonId) {
                                R.id.sort_food_modal_bottom_sheet_name_button -> {
                                    foodSorter = FoodSorter.NAME_ASC
                                }
                                R.id.sort_food_modal_bottom_sheet_expiration_date_button -> {
                                    foodSorter = FoodSorter.LIMIT_DATE_ASC
                                }
                                R.id.sort_food_modal_bottom_sheet_add_date_button -> {
                                    foodSorter = FoodSorter.ADD_DATE_ASC
                                }
                            }
                        }
                    }
                    R.id.sort_food_modal_bottom_sheet_descending_button -> {
                        if (isChecked) {
                            when (sortFoodModalBottomSheetRadioGroup.checkedRadioButtonId) {
                                R.id.sort_food_modal_bottom_sheet_name_button -> {
                                    foodSorter = FoodSorter.NAME_DESC
                                }
                                R.id.sort_food_modal_bottom_sheet_expiration_date_button -> {
                                    foodSorter = FoodSorter.LIMIT_DATE_DESC
                                }
                                R.id.sort_food_modal_bottom_sheet_add_date_button -> {
                                    foodSorter = FoodSorter.ADD_DATE_DESC
                                }
                            }
                        }
                    }
                }
                sortLiveEvent.postValue(foodSorter)
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        showDownArrowEvent.postValue(true)
        isOpen = false
        super.onDismiss(dialog)
    }
}
