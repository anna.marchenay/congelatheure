package com.marchenaya.presentation.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.marchenaya.data.model.FoodSorter
import com.marchenaya.presentation.R
import com.marchenaya.presentation.base.activity.BaseActivity
import com.marchenaya.presentation.databinding.ActivityMainBinding
import com.marchenaya.presentation.extensions.getColorFromAttr
import com.marchenaya.presentation.extensions.hide
import com.marchenaya.presentation.extensions.show
import com.marchenaya.presentation.utils.GlobalValueHelper

class MainActivity : BaseActivity<ActivityMainBinding>() {

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding =
        ActivityMainBinding::inflate

    private val navController by lazy { findNavController(getNavControllerId()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActivity()
    }

    private fun setupActivity() {
        setupOnDestinationChangeListener()
        binding {
            NavigationUI.setupWithNavController(activityMainNavigationBar, navController)
            activityMainToolbar.setupWithNavController(
                navController,
                AppBarConfiguration(
                    setOf(
                        R.id.food_list_fragment,
                        R.id.category_list_fragment,
                        R.id.location_list_fragment,
                        R.id.history_fragment
                    )
                )
            )
            activityMainNavigationBar.setupWithNavController(navController)
            setSupportActionBar(activityMainToolbar)
            activityMainToolbar.setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }

    private fun setupOnDestinationChangeListener() {
        binding {
            navController.addOnDestinationChangedListener { _, destination, _ ->
                if (destination.id == R.id.fullscreen_image_fragment) {
                    activityMainToolbar.hide()
                    activityMainNavigationBar.hide()
                } else {
                    activityMainToolbar.show()
                    activityMainNavigationBar.show()
                }
                if (destination.id == R.id.category_list_fragment ||
                    destination.id == R.id.location_list_fragment ||
                    destination.id == R.id.history_fragment
                ) {
                    GlobalValueHelper.globalFoodSorter = FoodSorter.NAME_ASC
                    GlobalValueHelper.globalFilteredCategoryIdList = listOf()
                }
            }
        }
    }

    fun getNavControllerId(): Int = R.id.activity_main_container

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        menu?.findItem(R.id.activity_main_menu_add)?.icon?.setTint(getColorFromAttr(R.attr.colorOnPrimary))
        menu?.findItem(R.id.activity_main_menu_search)?.icon?.setTint(getColorFromAttr(R.attr.colorOnPrimary))
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val addItem = menu?.findItem(R.id.activity_main_menu_add)
        val searchItem = menu?.findItem(R.id.activity_main_menu_search)

        super.onPrepareOptionsMenu(menu)
        binding {
            navController.addOnDestinationChangedListener { _, destination, _ ->
                when (destination.id) {
                    R.id.category_list_fragment, R.id.location_list_fragment -> {
                        addItem?.show()
                    }
                    R.id.food_list_fragment -> {
                        addItem?.show()
                        searchItem?.show()
                    }
                    R.id.history_fragment -> {
                        searchItem?.show()
                    }
                }
            }
        }
        return true
    }
}
