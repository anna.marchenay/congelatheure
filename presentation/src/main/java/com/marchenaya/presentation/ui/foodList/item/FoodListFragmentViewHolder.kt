package com.marchenaya.presentation.ui.foodList.item

import android.view.MotionEvent
import androidx.core.net.toUri
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.marchenaya.presentation.R
import com.marchenaya.presentation.component.dialog.DialogComponent
import com.marchenaya.presentation.databinding.FoodListItemBinding
import com.marchenaya.presentation.extensions.setCardBackgroundColorDependingOnLimitDate
import com.marchenaya.presentation.wrapper.FoodDataWrapper
import javax.inject.Inject

class FoodListFragmentViewHolder(private val binding: FoodListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    @Inject
    lateinit var dialogComponent: DialogComponent

    private val details = object : ItemDetailsLookup.ItemDetails<Long>() {
        var position: Long = 0
        override fun getPosition(): Int {
            return position.toInt()
        }

        override fun getSelectionKey(): Long {
            return position
        }

        override fun inSelectionHotspot(e: MotionEvent): Boolean {
            return false
        }

        override fun inDragRegion(e: MotionEvent): Boolean {
            return true
        }
    }

    fun bind(
        foodDataWrapper: FoodDataWrapper,
        position: Int,
        selectionTracker: SelectionTracker<Long>,
        onItemClickListener: (Int, String) -> Unit
    ) {
        with(binding) {
            details.position = position.toLong()
            Glide.with(itemView)
                .load(foodDataWrapper.getImagePath().toUri()).circleCrop()
                .placeholder(R.drawable.ic_image)
                .into(foodListItemImage)
            foodCard.setCardBackgroundColorDependingOnLimitDate(
                foodDataWrapper.getLimitDate(),
                itemView
            )

            foodListItemName.text = foodDataWrapper.getNameAndQuantity()

            foodCard.isChecked = selectionTracker.isSelected(details.selectionKey)
            root.setOnClickListener {
                onItemClickListener(foodDataWrapper.getId(), foodDataWrapper.getName())
            }
        }
    }

    fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> {
        return details
    }
}
