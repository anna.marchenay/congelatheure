package com.marchenaya.presentation.ui.categoryList.item

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemDetailsLookup.ItemDetails
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.marchenaya.presentation.component.dialog.DialogComponent
import com.marchenaya.presentation.databinding.CategoryListItemBinding
import com.marchenaya.presentation.wrapper.CategoryDataWrapper
import javax.inject.Inject

class CategoryListFragmentViewHolder(private val binding: CategoryListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    @Inject
    lateinit var dialogComponent: DialogComponent

    private val details = object : ItemDetailsLookup.ItemDetails<Long>() {
        var position: Long = 0
        override fun getPosition(): Int {
            return position.toInt()
        }

        override fun getSelectionKey(): Long {
            return position
        }

        override fun inSelectionHotspot(e: MotionEvent): Boolean {
            return false
        }

        override fun inDragRegion(e: MotionEvent): Boolean {
            return true
        }
    }

    fun bind(
        categoryDataWrapper: CategoryDataWrapper,
        position: Int,
        selectionTracker: SelectionTracker<Long>,
        onItemClickListener: (Int, String) -> Unit
    ) {
        with(binding) {
            categoryCard.isEnabled = categoryDataWrapper.getId() != 1
            details.position = position.toLong()
            categoryListItemName.text = categoryDataWrapper.getName()
            categoryCard.isChecked = selectionTracker.isSelected(details.selectionKey)
            root.setOnClickListener {
                onItemClickListener(categoryDataWrapper.getId(), categoryDataWrapper.getName())
            }
        }
    }

    fun getItemDetails(): ItemDetails<Long> {
        return details
    }
}
