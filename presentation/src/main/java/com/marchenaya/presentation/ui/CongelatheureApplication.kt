package com.marchenaya.presentation.ui

import android.app.Application
import com.marchenaya.data.di.DaggerDataComponent
import com.marchenaya.presentation.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class CongelatheureApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        DaggerApplicationComponent.factory()
            .create(this, DaggerDataComponent.factory().create(this)).inject(this)
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector
}
