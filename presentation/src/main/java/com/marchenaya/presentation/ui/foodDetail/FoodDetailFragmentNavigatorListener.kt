package com.marchenaya.presentation.ui.foodDetail

interface FoodDetailFragmentNavigatorListener {

    fun displayFoodListFragment()

    fun displayFullscreenImageFragment(imagePath: String, foodId: Int, foodName: String)

    fun displayUpdateFoodFragment(foodId: Int, foodName: String)
}
