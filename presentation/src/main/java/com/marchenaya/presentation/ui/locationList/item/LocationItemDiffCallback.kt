package com.marchenaya.presentation.ui.locationList.item

import androidx.recyclerview.widget.DiffUtil
import com.marchenaya.presentation.wrapper.LocationDataWrapper

class LocationItemDiffCallback : DiffUtil.ItemCallback<LocationDataWrapper>() {

    override fun areItemsTheSame(
        oldItem: LocationDataWrapper,
        newItem: LocationDataWrapper
    ): Boolean =
        oldItem.getId() == newItem.getId()

    override fun areContentsTheSame(
        oldItem: LocationDataWrapper,
        newItem: LocationDataWrapper
    ): Boolean =
        oldItem.getId() == newItem.getId() && oldItem.getName() == newItem.getName()
}
