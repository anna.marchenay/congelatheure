package com.marchenaya.presentation.ui.foodList.item

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.Selection
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.ListAdapter
import com.marchenaya.presentation.databinding.FoodListItemBinding
import com.marchenaya.presentation.wrapper.FoodDataWrapper
import javax.inject.Inject

class FoodListFragmentAdapter @Inject constructor() :
    ListAdapter<FoodDataWrapper, FoodListFragmentViewHolder>(FoodItemDiffCallback()) {

    private val items: MutableList<FoodDataWrapper> = mutableListOf()

    private lateinit var selectionTracker: SelectionTracker<Long>

    lateinit var onItemClickListener: (Int, String) -> Unit

    fun getCurrentFoodIds(position: Selection<Long>) = position.map {
        items[it.toInt()]
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FoodListFragmentViewHolder =
        FoodListFragmentViewHolder(
            FoodListItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: FoodListFragmentViewHolder, position: Int) {
        holder.bind(items[position], position, selectionTracker, onItemClickListener)
    }

    override fun getItemCount(): Int = items.size

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(newItems: List<FoodDataWrapper>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun setSelectionTracker(selectionTracker: SelectionTracker<Long>) {
        this.selectionTracker = selectionTracker
    }
}
