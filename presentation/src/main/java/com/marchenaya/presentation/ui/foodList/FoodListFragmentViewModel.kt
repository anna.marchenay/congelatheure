package com.marchenaya.presentation.ui.foodList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.marchenaya.data.model.FoodSorter
import com.marchenaya.data.repository.CategoryRepository
import com.marchenaya.data.repository.FoodRepository
import com.marchenaya.presentation.base.SingleLiveEvent
import com.marchenaya.presentation.base.viewmodel.BaseViewModel
import com.marchenaya.presentation.extensions.subscribeByIO
import com.marchenaya.presentation.wrapper.CategoryDataWrapper
import com.marchenaya.presentation.wrapper.FoodDataWrapper
import javax.inject.Inject
import timber.log.Timber

class FoodListFragmentViewModel @Inject constructor(
    private val foodRepository: FoodRepository,
    private val categoryRepository: CategoryRepository
) :
    BaseViewModel() {

    private val foodListLiveData = MutableLiveData<List<FoodDataWrapper>>()
    private val removedSingleFoodLiveEvent = SingleLiveEvent<String>()
    private val removedMultipleFoodsLiveEvent = SingleLiveEvent<String>()
    private val categoryListLiveData = MutableLiveData<List<CategoryDataWrapper>>()
    private val errorLiveEvent = SingleLiveEvent<Throwable>()

    fun getFoodListLiveData(): LiveData<List<FoodDataWrapper>> = foodListLiveData
    fun getRemovedSingleFoodLiveEvent(): LiveData<String> = removedSingleFoodLiveEvent
    fun getRemovedMultipleFoodsLiveEvent(): LiveData<String> = removedMultipleFoodsLiveEvent
    fun getCategoryListLiveData(): LiveData<List<CategoryDataWrapper>> = categoryListLiveData

    fun getErrorLiveEvent(): LiveData<Throwable> = errorLiveEvent

    init {
        retrieveFoodList()
        retrieveCategoryList()
    }

    private fun retrieveFoodList() {
        foodRepository.getFoodList().subscribeByIO(
            onSuccess = { foods ->
                foodListLiveData.postValue(
                    foods.map { FoodDataWrapper(it) }.sortedWith(
                        compareBy(
                            java.lang.String.CASE_INSENSITIVE_ORDER
                        ) { it.getName() }
                    )
                )
            },
            onError =
            {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }

    fun retrieveFoodListWithCriteria(
        foodSorter: FoodSorter = FoodSorter.NAME_ASC,
        filteredCategoryIdList: List<Int>,
        search: String
    ) {
        foodRepository.getFoodListWithCriteria(foodSorter, filteredCategoryIdList, search)
            .subscribeByIO(
                onSuccess = { foods ->
                    foodListLiveData.postValue(foods.map { FoodDataWrapper(it) })
                },
                onError =
                {
                    Timber.e(it)
                    errorLiveEvent.postValue(it)
                }
            )
    }

    fun removeFoods(foodToDeleteList: List<FoodDataWrapper>) {
        val foodToDeleteListString = foodToDeleteList.joinToString { it.getName() }

        foodRepository.removeFoods(foodToDeleteList.map { it.getId() })
            .subscribeByIO(
                onComplete = {
                    retrieveFoodList()
                    Timber.i("Foods deleted : $foodToDeleteList")
                    if (foodToDeleteList.count() == 1) {
                        removedSingleFoodLiveEvent.postValue(foodToDeleteListString)
                    } else {
                        removedMultipleFoodsLiveEvent.postValue(foodToDeleteListString)
                    }
                },
                onError = {
                    Timber.e(it)
                    errorLiveEvent.postValue(it)
                }
            )
    }

    private fun retrieveCategoryList() {
        categoryRepository.getCategoryList().subscribeByIO(
            onSuccess = { categories ->
                categoryListLiveData.postValue(categories.map { CategoryDataWrapper(it) })
            },
            onError = {
                Timber.e(it)
                errorLiveEvent.postValue(it)
            }
        )
    }
}
