package com.marchenaya.presentation.ui.categoryList.item

import androidx.recyclerview.widget.DiffUtil
import com.marchenaya.presentation.wrapper.CategoryDataWrapper

class CategoryItemDiffCallback : DiffUtil.ItemCallback<CategoryDataWrapper>() {

    override fun areItemsTheSame(
        oldItem: CategoryDataWrapper,
        newItem: CategoryDataWrapper
    ): Boolean =
        oldItem.getId() == newItem.getId()

    override fun areContentsTheSame(
        oldItem: CategoryDataWrapper,
        newItem: CategoryDataWrapper
    ): Boolean =
        oldItem.getId() == newItem.getId() && oldItem.getName() == newItem.getName()
}
