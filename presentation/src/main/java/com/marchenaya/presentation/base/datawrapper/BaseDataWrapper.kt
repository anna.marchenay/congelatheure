package com.marchenaya.presentation.base.datawrapper

interface BaseDataWrapper {

    fun getId(): Int

    fun getName(): String
}
