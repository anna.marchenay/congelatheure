package com.marchenaya.presentation.di.module

import com.marchenaya.presentation.di.annotation.PerActivity
import com.marchenaya.presentation.di.module.activity.MainActivityModule
import com.marchenaya.presentation.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityInjectorModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    fun mainActivityInjector(): MainActivity
}
