package com.marchenaya.presentation.di.module.activity

import androidx.navigation.findNavController
import com.marchenaya.presentation.di.annotation.PerActivity
import com.marchenaya.presentation.di.annotation.PerFragment
import com.marchenaya.presentation.ui.addFood.AddFoodFragment
import com.marchenaya.presentation.ui.addFood.AddFoodFragmentNavigatorListener
import com.marchenaya.presentation.ui.categoryList.CategoryListFragment
import com.marchenaya.presentation.ui.foodDetail.FoodDetailFragment
import com.marchenaya.presentation.ui.foodDetail.FoodDetailFragmentNavigatorListener
import com.marchenaya.presentation.ui.foodList.FoodListFragment
import com.marchenaya.presentation.ui.foodList.FoodListFragmentNavigatorListener
import com.marchenaya.presentation.ui.fullscreenImage.FullscreenImageFragment
import com.marchenaya.presentation.ui.fullscreenImage.FullscreenImageFragmentNavigatorListener
import com.marchenaya.presentation.ui.history.HistoryFragment
import com.marchenaya.presentation.ui.locationList.LocationListFragment
import com.marchenaya.presentation.ui.main.MainActivity
import com.marchenaya.presentation.ui.main.MainNavigator
import com.marchenaya.presentation.ui.sortFood.SortFoodModalBottomSheetFragment
import com.marchenaya.presentation.ui.updateFood.UpdateFoodFragment
import com.marchenaya.presentation.ui.updateFood.UpdateFoodFragmentNavigatorListener
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module(
    includes = [
        FoodListFragmentModule::class,
        CategoryListFragmentModule::class,
        LocationListFragmentModule::class,
        HistoryFragmentModule::class,
        AddFoodFragmentModule::class,
        FoodDetailFragmentModule::class,
        FullscreenImageFragmentModule::class,
        UpdateFoodFragmentModule::class,
        SortModalBottomSheetFragmentModule::class
    ]
)
class MainActivityModule {

    @PerActivity
    @Provides
    fun mainNavController(mainActivity: MainActivity) =
        mainActivity.findNavController(mainActivity.getNavControllerId())

    @PerActivity
    @Provides
    fun foodListFragmentNavigatorListener(mainNavigator: MainNavigator): FoodListFragmentNavigatorListener =
        mainNavigator

    @PerActivity
    @Provides
    fun addFoodFragmentNavigatorListener(mainNavigator: MainNavigator): AddFoodFragmentNavigatorListener =
        mainNavigator

    @PerActivity
    @Provides
    fun foodDetailFragmentNavigatorListener(mainNavigator: MainNavigator): FoodDetailFragmentNavigatorListener =
        mainNavigator

    @PerActivity
    @Provides
    fun fullscreenImageFragmentNavigatorListener(mainNavigator: MainNavigator): FullscreenImageFragmentNavigatorListener =
        mainNavigator

    @PerActivity
    @Provides
    fun updateFoodFragmentNavigatorListener(mainNavigator: MainNavigator): UpdateFoodFragmentNavigatorListener =
        mainNavigator
}

@Module
private interface FoodListFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun foodListFragmentInjector(): FoodListFragment
}

@Module
private interface CategoryListFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun categoryListFragmentInjector(): CategoryListFragment
}

@Module
private interface LocationListFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun locationListFragmentInjector(): LocationListFragment
}

@Module
private interface HistoryFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun historyFragmentInjector(): HistoryFragment
}

@Module
private interface AddFoodFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun addFoodFragmentInjector(): AddFoodFragment
}

@Module
private interface FoodDetailFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun foodDetailFragmentInjector(): FoodDetailFragment
}

@Module
private interface FullscreenImageFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun fullscreenImageFragmentInjector(): FullscreenImageFragment
}

@Module
private interface UpdateFoodFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun updateFoodFragmentInjector(): UpdateFoodFragment
}

@Module
private interface SortModalBottomSheetFragmentModule {

    @PerFragment
    @ContributesAndroidInjector
    fun sortModalBottomSheetFragmentInjector(): SortFoodModalBottomSheetFragment
}
