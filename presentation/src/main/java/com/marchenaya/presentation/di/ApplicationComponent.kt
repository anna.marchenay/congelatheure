package com.marchenaya.presentation.di

import android.content.Context
import com.marchenaya.data.di.DataComponent
import com.marchenaya.presentation.di.annotation.PerApplication
import com.marchenaya.presentation.di.module.ActivityInjectorModule
import com.marchenaya.presentation.di.module.ApplicationModule
import com.marchenaya.presentation.di.module.ViewModelModule
import com.marchenaya.presentation.ui.CongelatheureApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityInjectorModule::class,
        ViewModelModule::class,
        ApplicationModule::class
    ],
    dependencies = [
        DataComponent::class
    ]
)
interface ApplicationComponent {

    fun inject(application: CongelatheureApplication)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance context: Context,
            dataComponent: DataComponent
        ): ApplicationComponent
    }
}
