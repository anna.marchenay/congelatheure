package com.marchenaya.presentation.di.module

import androidx.lifecycle.ViewModel
import com.marchenaya.presentation.di.annotation.ViewModelKey
import com.marchenaya.presentation.ui.addFood.AddFoodFragmentViewModel
import com.marchenaya.presentation.ui.categoryList.CategoryListFragmentViewModel
import com.marchenaya.presentation.ui.foodDetail.FoodDetailFragmentViewModel
import com.marchenaya.presentation.ui.foodList.FoodListFragmentViewModel
import com.marchenaya.presentation.ui.history.HistoryFragmentViewModel
import com.marchenaya.presentation.ui.locationList.LocationListFragmentViewModel
import com.marchenaya.presentation.ui.updateFood.UpdateFoodFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FoodListFragmentViewModel::class)
    fun bindFoodListFragmentViewModel(viewModel: FoodListFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoryListFragmentViewModel::class)
    fun bindCategoryListFragmentViewModel(viewModel: CategoryListFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LocationListFragmentViewModel::class)
    fun bindLocationListFragmentViewModel(viewModel: LocationListFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HistoryFragmentViewModel::class)
    fun bindHistoryFragmentViewModel(viewModel: HistoryFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddFoodFragmentViewModel::class)
    fun bindAddFoodFragmentViewModel(viewModel: AddFoodFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FoodDetailFragmentViewModel::class)
    fun bindFoodDetailFragmentViewModel(viewModel: FoodDetailFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UpdateFoodFragmentViewModel::class)
    fun bindUpdateFoodFragmentViewModel(viewModel: UpdateFoodFragmentViewModel): ViewModel
}
