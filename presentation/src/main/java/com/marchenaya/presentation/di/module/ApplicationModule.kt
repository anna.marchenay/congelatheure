package com.marchenaya.presentation.di.module

import com.marchenaya.presentation.component.dialog.DialogComponent
import com.marchenaya.presentation.component.dialog.DialogComponentImpl
import com.marchenaya.presentation.component.file.FileComponent
import com.marchenaya.presentation.component.file.FileComponentImpl
import com.marchenaya.presentation.component.intent.IntentComponent
import com.marchenaya.presentation.component.intent.IntentComponentImpl
import com.marchenaya.presentation.component.materialcardview.MaterialCardViewComponent
import com.marchenaya.presentation.component.materialcardview.MaterialCardViewComponentImpl
import com.marchenaya.presentation.component.navigation.NavigationComponent
import com.marchenaya.presentation.component.navigation.NavigationComponentImpl
import com.marchenaya.presentation.component.snackbar.SnackbarComponent
import com.marchenaya.presentation.component.snackbar.SnackbarComponentImpl
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
object ApplicationModule {

    @Provides
    @Reusable
    fun dialogComponent(dialogComponent: DialogComponentImpl): DialogComponent = dialogComponent

    @Provides
    @Reusable
    fun materialCardViewComponent(materialCardViewComponent: MaterialCardViewComponentImpl): MaterialCardViewComponent =
        materialCardViewComponent

    @Provides
    @Reusable
    fun snackbarComponent(snackbarComponent: SnackbarComponentImpl): SnackbarComponent =
        snackbarComponent

    @Provides
    @Reusable
    fun navigationComponent(navigationComponent: NavigationComponentImpl): NavigationComponent =
        navigationComponent

    @Provides
    @Reusable
    fun fileComponent(fileComponent: FileComponentImpl): FileComponent =
        fileComponent

    @Provides
    @Reusable
    fun intentComponent(intentComponent: IntentComponentImpl): IntentComponent =
        intentComponent
}
